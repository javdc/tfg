package com.fashionlint.android.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.fashionlint.android.utils.ErrorEnum
import com.fashionlint.android.R
import com.fashionlint.android.activity.CreateOutfitActivity.Companion.EXTRA_STYLE
import com.fashionlint.android.activity.OutfitDetailActivity.Companion.EXTRA_ID
import com.fashionlint.android.activity.OutfitDetailActivity.Companion.EXTRA_POS
import com.fashionlint.android.utils.Resource
import com.fashionlint.android.adapter.OutfitAdapter
import com.fashionlint.android.database.OutfitDomain
import com.fashionlint.android.fragment.OutfitsListFragment
import com.fashionlint.android.viewmodel.OutfitRecommendationsViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_outfit_recommendations.*
import kotlin.math.roundToInt

class OutfitRecommendationsActivity : AppCompatActivity(), OutfitAdapter.OutfitsListAdapterActions {

    private val outfitRecommendationsViewModel
            by lazy { ViewModelProvider(this).get(OutfitRecommendationsViewModel::class.java) }
    private lateinit var adapter: OutfitAdapter

    private val outfitsRecommendationsObserver: Observer<Resource<List<OutfitDomain>>> = Observer { resource ->
        when (resource) {
            is Resource.Success -> resource.data?.let { outfits ->
                adapter.setOutfits(outfits)
            }
            is Resource.Error -> {
                when(resource.error) {
                    ErrorEnum.SERVER_ERROR -> showSnackbar(R.string.error__server)
                    ErrorEnum.RETROFIT_ERROR -> showSnackbar(R.string.error__retrofit)
                    ErrorEnum.NO_OUTFITS -> {
                        adapter.setOutfits(mutableListOf())
                    }
                    else -> showSnackbar(R.string.error__unspecified)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_outfit_recommendations)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupRecyclerView()

        setCalculatedStylesText()

        val style = intent.getStringExtra(EXTRA_STYLE)
        outfitRecommendationsViewModel.getRecommendedOutfits("$style").observe(this, outfitsRecommendationsObserver)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupRecyclerView() {
        adapter = OutfitAdapter(ArrayList())
        adapter.setListener(this)
        adapter.setHasStableIds(true)
        outfit_recommendations__recycler_view.adapter = adapter
    }

    private fun showSnackbar(resId: Int) {
        Snackbar.make(outfit_recommendations__recycler_view, resId, Snackbar.LENGTH_SHORT).show()
    }

    private fun setCalculatedStylesText() {
        val casualRatio = intent.getDoubleExtra(CreateOutfitActivity.EXTRA_CASUAL_RATIO, 0.0)
        val formalRatio = intent.getDoubleExtra(CreateOutfitActivity.EXTRA_FORMAL_RATIO, 0.0)
        val sportRatio = intent.getDoubleExtra(CreateOutfitActivity.EXTRA_SPORT_RATIO, 0.0)
        val casualPercent = (casualRatio * 100).roundToInt()
        val formalPercent = (formalRatio * 100).roundToInt()
        val sportPercent = (sportRatio * 100).roundToInt()
        val calculatedStylesText = "$casualPercent% casual\n$formalPercent% formal\n$sportPercent% sport"
        outfit_recommendations__label__calculated_styles.text = calculatedStylesText
    }

    override fun onClickOutfit(id: Int, position: Int) {
        val detailIntent = Intent(this, OutfitDetailActivity::class.java)
        detailIntent.putExtra(EXTRA_ID, id)
        detailIntent.putExtra(EXTRA_POS, position)
        startActivityForResult(detailIntent, OutfitsListFragment.DETAILS)
    }

    override fun onFavoriteOutfit(id: Int) {
        outfitRecommendationsViewModel.setFavoriteById(id)
    }

    override fun onUnfavoriteOutfit(id: Int) {
        outfitRecommendationsViewModel.unsetFavoriteById(id)
    }
}