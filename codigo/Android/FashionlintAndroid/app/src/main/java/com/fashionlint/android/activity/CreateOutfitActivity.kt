package com.fashionlint.android.activity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.bumptech.glide.Glide
import com.fashionlint.android.utils.ErrorEnum
import com.fashionlint.android.R
import com.fashionlint.android.utils.Resource
import com.fashionlint.android.api.CalculatedStylesDto
import com.fashionlint.android.api.OutfitDto
import com.fashionlint.android.viewmodel.CreateOutfitViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_create_outfit.*

class CreateOutfitActivity : AppCompatActivity() {

    private val createOutfitViewModel
            by lazy { ViewModelProvider(this).get(CreateOutfitViewModel::class.java) }

    private val apiBaseUrl by lazy { PreferenceManager.getDefaultSharedPreferences(application)
        ?.getString(SettingsActivity.HOST_KEY, application.getString(R.string.settings__default_host))
        ?: application.getString(R.string.settings__default_host) }

    private var uploadedImageFilename: String? = null

    private val imageUploadedObserver: Observer<Resource<String>> = Observer { resource ->
        when (resource) {
            is Resource.Loading -> {
                create_outfit__btn__ok.isEnabled = false
                create_outfit__btn__ok.alpha = 0.25f
                create_outfit__loading__photo_upload.visibility = View.VISIBLE
            }
            is Resource.Success -> {
                create_outfit__loading__photo_upload.visibility = View.GONE
                resource.data?.let { filename ->
                    showPhoto("$apiBaseUrl/public/$filename")
                    create_outfit__btn__ok.isEnabled = true
                    create_outfit__btn__ok.alpha = 1f
                    uploadedImageFilename = filename
                }
            }
            is Resource.Error -> {
                create_outfit__loading__photo_upload.visibility = View.GONE
                create_outfit__btn__ok.isEnabled = false
                create_outfit__btn__ok.alpha = 0.25f
                when(resource.error) {
                    ErrorEnum.IMAGE_LOADING_ERROR -> showSnackbar(R.string.error__image_upload)
                    ErrorEnum.RETROFIT_ERROR -> showSnackbar(R.string.error__retrofit)
                    ErrorEnum.SERVER_ERROR -> showSnackbar(R.string.error__server)
                    else -> showSnackbar(R.string.error__unspecified)
                }
            }
        }
    }

    private val createOutfitObserver: Observer<Resource<CalculatedStylesDto>> = Observer { resource ->
        when (resource) {
            is Resource.Loading -> create_outfit__btn__ok.isEnabled = false
            is Resource.Success -> {
                resource.data?.let { calculatedStyles ->
                    openRecommendationsActivity(calculatedStyles)
                    setResult(RESULT_OK, intent)
                    finish()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_outfit)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        intent.getStringExtra("EXTRA_PHOTO_URI")?.let {
            showPhoto(it)
            createOutfitViewModel.uploadImage(it.toUri()).observe(this, imageUploadedObserver)
        }
        create_outfit__btn__cancel.setOnClickListener { finish() }
        create_outfit__btn__ok.setOnClickListener { saveOutfit() }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun saveOutfit() {
        if(validateFields()) {

            val outfit = OutfitDto(
                id = 0,
                name = create_outfit__input__name.text.toString(),
                brand = null,
                source = null, // TODO: relación usuario y outfit
                description = create_outfit__input__description.text.toString(),
                style = null,
                price = null,
                currency = null,
                image = uploadedImageFilename,
                link = null
            )
            createOutfitViewModel.postOutfit(outfit).observe(this, createOutfitObserver)
        }
    }

    private fun validateFields(): Boolean {
        var validation = true

        if(create_outfit__input__name.text.toString().trim().isEmpty()) {
            create_outfit__input_layout__name.error = getString(R.string.error__name_empty)
            validation = false
        } else {
            create_outfit__input_layout__name.isErrorEnabled = false
        }

        if(create_outfit__input__name.text.toString().length > 250) {
            validation = false
        }

        return validation
    }

    private fun showPhoto(uri: String?) {
        Glide.with(this)
            .load(uri)
            .into(create_outfit__img__photo)
    }

    private fun openRecommendationsActivity(calculatedStyles: CalculatedStylesDto) {
        val map = mapOf(Pair("casual", calculatedStyles.casual), Pair("formal", calculatedStyles.formal), Pair("sport", calculatedStyles.sport))
        val style = map.maxByOrNull { it.value }?.key

        val detailIntent = Intent(this, OutfitRecommendationsActivity::class.java)
        detailIntent.putExtra(EXTRA_STYLE, style)
        detailIntent.putExtra(EXTRA_CASUAL_RATIO, calculatedStyles.casual)
        detailIntent.putExtra(EXTRA_FORMAL_RATIO, calculatedStyles.formal)
        detailIntent.putExtra(EXTRA_SPORT_RATIO, calculatedStyles.sport)
        startActivity(detailIntent)
    }

    private fun showSnackbar(resId: Int) {
        Snackbar.make(create_outfit__btn__ok, resId, Snackbar.LENGTH_SHORT).show()
    }

    companion object {
        const val EXTRA_STYLE = "style"
        const val EXTRA_CASUAL_RATIO = "casual_ratio"
        const val EXTRA_FORMAL_RATIO = "formal_ratio"
        const val EXTRA_SPORT_RATIO = "sport_ratio"
    }
}