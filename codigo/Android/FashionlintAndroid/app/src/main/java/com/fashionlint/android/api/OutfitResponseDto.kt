package com.fashionlint.android.api

import com.fashionlint.android.api.CalculatedStylesDto
import com.fashionlint.android.api.OutfitDto
import com.google.gson.annotations.SerializedName

data class OutfitResponseDto(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("outfit")
    val outfit: OutfitDto,
    @SerializedName("calculated_styles")
    val calculatedStyles: CalculatedStylesDto?
)