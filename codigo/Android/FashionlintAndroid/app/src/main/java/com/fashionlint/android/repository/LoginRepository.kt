package com.fashionlint.android.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fashionlint.android.api.*
import com.fashionlint.android.utils.ErrorEnum
import com.fashionlint.android.utils.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.awaitResponse
import kotlin.coroutines.CoroutineContext


class LoginRepository(apiBaseUrl: String, private val job: Job): CoroutineScope {

    private val fashionlintWs = RetrofitInstance.retrofit(apiBaseUrl).create(FashionlintWs::class.java)

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO

    fun login(username: String, password: String): LiveData<Resource<LoginResponseDto>> {
        val liveData = MutableLiveData<Resource<LoginResponseDto>>()
        launch {
            liveData.postValue(Resource.Loading())
            try {
                fashionlintWs.login(LoginDto(username, password)).awaitResponse().let { response ->
                    if (response.isSuccessful) {
                        val loginResponse = response.body()
                        liveData.postValue(Resource.Success(loginResponse))
                    } else {
                        when (response.code()) {
                            400 -> liveData.postValue(Resource.Error(ErrorEnum.VALIDATIONS_FAILED))
                            401 -> liveData.postValue(Resource.Error(ErrorEnum.INCORRECT_PASSWORD))
                            404 -> liveData.postValue(Resource.Error(ErrorEnum.NOT_REGISTERED))
                            else -> liveData.postValue(Resource.Error(ErrorEnum.SERVER_ERROR))
                        }
                    }
                }
            } catch (ex: Exception) {
                liveData.postValue(Resource.Error(ErrorEnum.RETROFIT_ERROR))
                Log.e("RETROFIT", "Error connecting to server", ex)
            }
        }
        return liveData
    }

    fun register(username: String, name:String, password: String): LiveData<Resource<LoginResponseDto>> {
        val liveData = MutableLiveData<Resource<LoginResponseDto>>()
        launch {
            liveData.postValue(Resource.Loading())
            try {
                fashionlintWs.register(RegisterDto(username, name, password)).awaitResponse().let { response ->
                    if (response.isSuccessful) {
                        val loginResponse = response.body()
                        liveData.postValue(Resource.Success(loginResponse))
                    } else {
                        when (response.code()) {
                            400 -> liveData.postValue(Resource.Error(ErrorEnum.VALIDATIONS_FAILED))
                            401 -> liveData.postValue(Resource.Error(ErrorEnum.INCORRECT_PASSWORD))
                            404 -> liveData.postValue(Resource.Error(ErrorEnum.NOT_REGISTERED))
                            else -> liveData.postValue(Resource.Error(ErrorEnum.SERVER_ERROR))
                        }
                    }
                }
            } catch (ex: Exception) {
                liveData.postValue(Resource.Error(ErrorEnum.RETROFIT_ERROR))
                Log.e("RETROFIT", "Error connecting to server", ex)
            }
        }
        return liveData
    }

}