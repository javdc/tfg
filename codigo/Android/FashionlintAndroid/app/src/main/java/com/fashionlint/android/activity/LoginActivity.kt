package com.fashionlint.android.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager.getDefaultSharedPreferences
import com.fashionlint.android.utils.ErrorEnum
import com.fashionlint.android.R
import com.fashionlint.android.utils.Resource
import com.fashionlint.android.api.LoginResponseDto
import com.fashionlint.android.viewmodel.LoginViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private var registerMode = false

    private val loginObserver: Observer<Resource<LoginResponseDto>> = Observer { resource ->
        when (resource) {
            is Resource.Loading -> login__btn__login.isEnabled = false
            is Resource.Success -> {
                resource.data?.let { loginResponse ->
                    saveLoginInfo(
                        loginResponse.user.username,
                        loginResponse.user.name,
                        loginResponse.accessToken
                    )
                    openMainActivity()
                }
            }
            is Resource.Error -> {
                login__btn__login.isEnabled = true
                resource.error?.let {error ->
                    when(error) {
                        ErrorEnum.VALIDATIONS_FAILED -> showSnackbar(R.string.error__validations_failed)
                        ErrorEnum.INCORRECT_PASSWORD -> showSnackbar(R.string.error__incorrect_password)
                        ErrorEnum.NOT_REGISTERED -> showSnackbar(R.string.error__not_registered)
                        ErrorEnum.RETROFIT_ERROR -> showSnackbar(R.string.error__retrofit)
                        ErrorEnum.SERVER_ERROR -> showSnackbar(R.string.error__server)
                        else -> showSnackbar(R.string.error__unspecified)
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        login__btn__login.setOnClickListener { loginOrRegister() }
        login__link__register.setOnClickListener { setupRegisterMode() }
        login__btn__settings.setOnClickListener { startSettingsActivity() }
    }

    private fun saveLoginInfo(username: String, name: String, accessToken: String) {
        val sharedPreferences = getDefaultSharedPreferences(this)
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(USERNAME_KEY, username)
            .putString(NAME_KEY, name)
            .putString(NAME_KEY, name)
            .putString(TOKEN_KEY, accessToken)
            .apply()
    }

    private fun openMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun startSettingsActivity(): Boolean {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
        return true
    }

    private fun setupRegisterMode() {
        registerMode = true
        login__btn__login.text = getString(R.string.general__register)
        login__input_layout__name.visibility = View.VISIBLE
        login__link__register.visibility = View.GONE
    }

    private fun loginOrRegister() {
        if (!registerMode) {
            loginViewModel.login(
                login__input__username.text.toString(),
                login__input__password.text.toString()
            ).observe(this, loginObserver)
        } else {
            loginViewModel.register(
                login__input__username.text.toString(),
                login__input__name.text.toString(),
                login__input__password.text.toString()
            ).observe(this, loginObserver)
        }
    }

    private fun showSnackbar(resId: Int) {
        Snackbar.make(login__btn__login, resId, Snackbar.LENGTH_LONG).show()
    }

    companion object {
        const val USERNAME_KEY = "username"
        const val NAME_KEY = "name"
        const val TOKEN_KEY = "token"
    }
}