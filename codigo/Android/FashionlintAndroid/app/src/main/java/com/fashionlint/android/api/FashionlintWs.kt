package com.fashionlint.android.api

import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface FashionlintWs {
    @POST("/api/auth/login")
    fun login(@Body login: LoginDto): Call<LoginResponseDto>

    @POST("/api/auth/register")
    fun register(@Body register: RegisterDto): Call<LoginResponseDto>

    @GET("/api/outfits")
    fun getOutfits(): Call<OutfitsResponseDto>

    @Multipart
    @POST("/api/outfits/image_upload")
    fun uploadImage(@Header("Authorization") bearer: String, @Part imagePart: MultipartBody.Part): Call<ImageUploadResponseDto>

    @POST("/api/outfits")
    fun postOutfit(@Header("Authorization") bearer: String, @Body outfit: OutfitDto): Call<OutfitResponseDto>
}