package com.fashionlint.android.api

import com.google.gson.annotations.SerializedName

data class UserDto(
    @SerializedName("username")
    val username: String,
    @SerializedName("name")
    val name: String
)