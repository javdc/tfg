package com.fashionlint.android.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.preference.PreferenceManager
import com.fashionlint.android.R
import com.fashionlint.android.utils.Resource
import com.fashionlint.android.activity.SettingsActivity.Companion.HOST_KEY
import com.fashionlint.android.api.LoginResponseDto
import com.fashionlint.android.repository.LoginRepository
import kotlinx.coroutines.Job

class LoginViewModel(application: Application) : AndroidViewModel(application) {

    private val loginRepository: LoginRepository
    private val job = Job()

    init {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(application)
        val apiBaseUrl = sharedPreferences.getString(
            HOST_KEY,
            application.getString(R.string.settings__default_host)
        ) ?: application.getString(R.string.settings__default_host)
        loginRepository = LoginRepository(apiBaseUrl, job)
    }

    override fun onCleared() {
        job.cancel()
        super.onCleared()
    }

    fun login(username: String, password: String): LiveData<Resource<LoginResponseDto>> {
        return loginRepository.login(username, password)
    }

    fun register(username: String, name: String, password: String): LiveData<Resource<LoginResponseDto>> {
        return loginRepository.register(username, name, password)
    }

}