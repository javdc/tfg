package com.fashionlint.android.adapter

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fashionlint.android.R
import com.fashionlint.android.database.OutfitDomain
import kotlinx.android.synthetic.main.row_outfit_small.view.*

class OutfitSmallAdapter(outfits: List<OutfitDomain>) : RecyclerView.Adapter<OutfitSmallAdapter.ViewHolder>() {

    private var outfitsData = mutableListOf<OutfitDomain>()
    private var listener: OutfitsListAdapterActions? = null

    init {
        outfitsData.addAll(outfits)
    }

    interface OutfitsListAdapterActions {
        fun onClickOutfit(id: Int, position: Int)
    }

    fun setListener(listener: OutfitsListAdapterActions?) {
        this.listener = listener
    }

    fun setOutfits(outfits: List<OutfitDomain>) {
        this.outfitsData = outfits as MutableList<OutfitDomain>
        notifyDataSetChanged()
    }

    fun addOutfit(position: Int, outfit: OutfitDomain) {
        this.outfitsData.add(position, outfit)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_outfit_small,
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = outfitsData[position]
        holder.bindTo(current)
    }

    override fun getItemCount(): Int {
        return outfitsData.size
    }

    override fun getItemId(position: Int): Long {
        return outfitsData[position].id.toLong()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        fun bindTo(outfit: OutfitDomain) {
            setOutfitInfo(outfit)
            setPhoto(outfit)
        }

        private fun setOutfitInfo(outfit: OutfitDomain) {
            itemView.row_outfit_small__label__name.text = outfit.name
            itemView.row_outfit_small__label__description.text = outfit.description
        }

        private fun setPhoto(outfit: OutfitDomain) {
            outfit.images?.getOrNull(0)?.let { image ->
                Glide.with(itemView.context)
                    .load(image)
                    .placeholder(ColorDrawable(Color.parseColor("#cccccc")))
                    .centerCrop()
                    .into(itemView.row_outfit_small__img__photo)
            }
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            val id = outfitsData[position].id
            listener?.onClickOutfit(id, position)
        }

    }

}