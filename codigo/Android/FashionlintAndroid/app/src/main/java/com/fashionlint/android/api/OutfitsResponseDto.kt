package com.fashionlint.android.api

import com.fashionlint.android.api.OutfitDto
import com.google.gson.annotations.SerializedName

data class OutfitsResponseDto(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("outfits")
    val outfits: List<OutfitDto>
)