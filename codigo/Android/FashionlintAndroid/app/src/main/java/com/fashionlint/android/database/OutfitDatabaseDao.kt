package com.fashionlint.android.database

import androidx.room.*

@Dao
interface OutfitDatabaseDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(outfit: OutfitDomain)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(outfits: List<OutfitDomain>)

    @Update
    suspend fun update(outfit: OutfitDomain)

    @Query("SELECT * FROM outfits WHERE id = :key")
    suspend fun get(key: Int): OutfitDomain?

    @Query("SELECT * FROM outfits ORDER BY id DESC")
    suspend fun getAll(): List<OutfitDomain>

    @Query("SELECT * FROM outfits ORDER BY name ASC")
    suspend fun getAllOrderByName(): List<OutfitDomain>

    @Query("SELECT * FROM outfits WHERE favorite ORDER BY id DESC")
    suspend fun getFavorites(): List<OutfitDomain>

    @Query("SELECT * FROM outfits WHERE source = :username ORDER BY id DESC")
    suspend fun getFromUser(username: String): List<OutfitDomain>

    @Query("SELECT id FROM outfits WHERE favorite")
    suspend fun getFavoriteIds(): List<Int>

    @Query("UPDATE outfits SET favorite = 1 WHERE id = :key")
    suspend fun setFavoriteById(key: Int)

    @Query("UPDATE outfits SET favorite = 0 WHERE id = :key")
    suspend fun unsetFavoriteById(key: Int)

    @Query("SELECT * FROM outfits WHERE style LIKE :style ORDER BY RANDOM() LIMIT 30")
    suspend fun getByStyle(style: String): List<OutfitDomain>

}