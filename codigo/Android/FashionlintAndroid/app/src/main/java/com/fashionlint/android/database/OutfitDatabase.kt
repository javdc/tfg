package com.fashionlint.android.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [OutfitDomain::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class OutfitDatabase : RoomDatabase() {

    abstract val outfitDatabaseDao: OutfitDatabaseDao

    companion object {
        @Volatile
        private var INSTANCE: OutfitDatabase? = null

        fun getInstance(context: Context): OutfitDatabase {
            synchronized(this) {
                var instance =
                    INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        OutfitDatabase::class.java,
                        "outfits_database"
                    )
                        .fallbackToDestructiveMigration()
                        // .allowMainThreadQueries()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }

}