package com.fashionlint.android.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.fashionlint.android.utils.ErrorEnum
import com.fashionlint.android.activity.OutfitDetailActivity
import com.fashionlint.android.viewmodel.MyOutfitsListViewModel
import com.fashionlint.android.R
import com.fashionlint.android.utils.Resource
import com.fashionlint.android.activity.OutfitDetailActivity.Companion.EXTRA_ID
import com.fashionlint.android.activity.OutfitDetailActivity.Companion.EXTRA_POS
import com.fashionlint.android.database.OutfitDomain
import com.fashionlint.android.adapter.OutfitSmallAdapter
import com.fashionlint.android.fragment.OutfitsListFragment.Companion.DETAILS
import kotlinx.android.synthetic.main.fragment_my_outfits_list.*
import java.util.ArrayList

class MyOutfitsListFragment : Fragment(), OutfitSmallAdapter.OutfitsListAdapterActions {

    private val myOutfitsListViewModel by lazy { ViewModelProvider(this).get(
        MyOutfitsListViewModel::class.java) }
    private lateinit var adapter: OutfitSmallAdapter

    private val outfitsListObserver: Observer<Resource<List<OutfitDomain>>> = Observer { resource ->
        when (resource) {
            is Resource.Success -> resource.data?.let { outfits ->
                my_outfit_list__container__no_outfit.visibility = View.GONE
                adapter.setOutfits(outfits)
            }
            is Resource.Error -> {
                when (resource.error) {
                    ErrorEnum.NO_OUTFITS -> {
                        my_outfit_list__container__no_outfit.visibility = View.VISIBLE
                        adapter.setOutfits(mutableListOf())
                    }
                    else -> showSnackbar(R.string.error__unspecified)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_outfits_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupRecyclerView()
        myOutfitsListViewModel.getMyOutfits().observe(viewLifecycleOwner, outfitsListObserver)
    }

    private fun setupRecyclerView() {
        adapter = OutfitSmallAdapter(ArrayList())
        adapter.setListener(this)
        adapter.setHasStableIds(true)
        my_outfit_list__recycler_view.adapter = adapter
        my_outfit_list__recycler_view.layoutManager = LinearLayoutManager(context)
    }

    private fun showSnackbar(resId: Int) {
        Snackbar.make(my_outfit_list__recycler_view, resId, Snackbar.LENGTH_SHORT).show()
    }

    override fun onClickOutfit(id: Int, position: Int) {
        val detailIntent = Intent(context, OutfitDetailActivity::class.java)
        detailIntent.putExtra(EXTRA_ID, id)
        detailIntent.putExtra(EXTRA_POS, position)
        startActivityForResult(detailIntent, DETAILS)
    }
}