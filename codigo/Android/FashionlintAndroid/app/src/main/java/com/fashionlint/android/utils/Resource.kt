package com.fashionlint.android.utils

sealed class Resource<T>(
        val data: T? = null,
        val message: String? = null,
        val error: ErrorEnum? = null
) {
    class Success<T>(data: T? = null) : Resource<T>(data)
    class Loading<T>(data: T? = null) : Resource<T>(data)
    class Error<T>(error: ErrorEnum? = null, message: String? = null, data: T? = null) : Resource<T>(data, message, error)
}