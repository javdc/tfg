package com.fashionlint.android.api

import com.google.gson.annotations.SerializedName

data class LoginDto(
    @SerializedName("username")
    val username: String,
    @SerializedName("password")
    val password: String
)