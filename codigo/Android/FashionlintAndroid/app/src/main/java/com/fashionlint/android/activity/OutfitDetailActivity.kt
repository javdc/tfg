package com.fashionlint.android.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.like.LikeButton
import com.like.OnLikeListener
import com.fashionlint.android.R
import com.fashionlint.android.adapter.OutfitImagesAdapter
import com.fashionlint.android.database.OutfitDomain
import com.fashionlint.android.viewmodel.OutfitDetailViewModel
import kotlinx.android.synthetic.main.activity_outfit_detail.*


class OutfitDetailActivity : AppCompatActivity() {

    private val outfitDetailViewModel by lazy { ViewModelProvider(this).get(
        OutfitDetailViewModel::class.java) }
    private lateinit var outfit: OutfitDomain
    private var position: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_outfit_detail)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = title

        val id = intent.getIntExtra(EXTRA_ID, -1)

        this.position = intent.getIntExtra(EXTRA_POS, 0)

        outfitDetailViewModel.getOutfit(id).observe(this, { outfit ->
            outfit?.let {
                this.outfit = outfit
                setBasicInfo()
                setImages()
                setInformationButton()
                setFavoriteIcon()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setBasicInfo() {
        detail__label__name.text = outfit.name
        detail__label__creator.text = outfit.source
        detail__label__description.text = outfit.description
    }

    private fun setImages() {
        outfit.images?.let { images ->
            detail__recycler__images.setHasFixedSize(true)
            detail__recycler__images.setItemViewCacheSize(4)
            detail__recycler__images.adapter =
                OutfitImagesAdapter(images)
            detail__recycler__images.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            PagerSnapHelper().attachToRecyclerView(detail__recycler__images)
        }
    }

    private fun setInformationButton() {
        outfit.link?.let { url ->
            detail__btn__info.setOnClickListener {
                val uri: Uri = Uri.parse(url)
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)
            }
        } ?: run {
            detail__btn__info.visibility = View.GONE
        }
    }

    private fun setFavoriteIcon() {
        detail__like_btn__favorite.isLiked = outfit.favorite
        detail__like_btn__favorite.setOnLikeListener(object : OnLikeListener {
            override fun liked(likeButton: LikeButton) {
                outfitDetailViewModel.setFavoriteById(outfit.id)
                setResult(RESULT_FAVORITE_CHANGED, intent)
            }
            override fun unLiked(likeButton: LikeButton) {
                outfitDetailViewModel.unsetFavoriteById(outfit.id)
                setResult(RESULT_FAVORITE_CHANGED, intent)
            }
        })
    }

    companion object {
        const val EXTRA_ID = "id"
        const val EXTRA_POS = "pos"
        const val RESULT_FAVORITE_CHANGED = 1
    }

}