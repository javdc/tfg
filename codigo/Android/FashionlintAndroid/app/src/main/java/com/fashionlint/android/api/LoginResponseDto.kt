package com.fashionlint.android.api

import com.google.gson.annotations.SerializedName

data class LoginResponseDto(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("user")
    val user: UserDto
)