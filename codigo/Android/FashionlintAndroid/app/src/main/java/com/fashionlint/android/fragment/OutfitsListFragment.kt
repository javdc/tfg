package com.fashionlint.android.fragment

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.fashionlint.android.utils.ErrorEnum
import com.fashionlint.android.R
import com.fashionlint.android.utils.Resource
import com.fashionlint.android.activity.CameraActivity
import com.fashionlint.android.activity.OutfitDetailActivity
import com.fashionlint.android.activity.OutfitDetailActivity.Companion.EXTRA_ID
import com.fashionlint.android.activity.OutfitDetailActivity.Companion.EXTRA_POS
import com.fashionlint.android.activity.OutfitDetailActivity.Companion.RESULT_FAVORITE_CHANGED
import com.fashionlint.android.adapter.OutfitAdapter
import com.fashionlint.android.database.OutfitDomain
import com.fashionlint.android.viewmodel.OutfitsListViewModel
import kotlinx.android.synthetic.main.fragment_outfits_list.*

class OutfitsListFragment : Fragment(), OutfitAdapter.OutfitsListAdapterActions {

    private val outfitsListViewModel by lazy { ViewModelProvider(this).get(OutfitsListViewModel::class.java) }
    private lateinit var adapter: OutfitAdapter

    private val outfitsListObserver: Observer<Resource<List<OutfitDomain>>> = Observer { resource ->
        when (resource) {
            is Resource.Loading -> {
                setLoadingIndicator(true)
                resource.data?.let { outfits ->
                    adapter.setOutfits(outfits)
                }
            }
            is Resource.Success -> resource.data?.let { outfits ->
                adapter.setOutfits(outfits)
                setLoadingIndicator(false)
            }
            is Resource.Error -> {
                when (resource.error) {
                    ErrorEnum.SERVER_ERROR -> showSnackbar(R.string.error__server)
                    ErrorEnum.RETROFIT_ERROR -> showSnackbar(R.string.error__retrofit)
                    ErrorEnum.NO_OUTFITS -> adapter.setOutfits(mutableListOf())
                    else -> showSnackbar(R.string.error__unspecified)
                }
                resource.data?.let { outfits ->
                    adapter.setOutfits(outfits)
                }
                setLoadingIndicator(false)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_outfits_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupFAB()
        setupSwipeRefreshLayout()
        setupRecyclerView()
        outfitsListViewModel.getOutfitsFromApi().observe(viewLifecycleOwner, outfitsListObserver)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        activity?.menuInflater?.inflate(R.menu.list_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.action_order_by_new -> outfitsListViewModel.getOutfits().observe(viewLifecycleOwner, outfitsListObserver)
            R.id.action_order_by_name -> outfitsListViewModel.getOutfitsOrderedByName().observe(viewLifecycleOwner, outfitsListObserver)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == DETAILS && resultCode == RESULT_FAVORITE_CHANGED) {
            outfitsListViewModel.getOutfits().observe(viewLifecycleOwner, outfitsListObserver)
        }
    }

    private fun setupFAB() {
        outfit_list__fab.setOnClickListener {
            val cameraIntent = Intent(context, CameraActivity::class.java)
            startActivityForResult(cameraIntent, CAMERA)
        }
    }

    private fun setupSwipeRefreshLayout() {
        outfit_list__swipe_refresh.isRefreshing = false
        outfit_list__swipe_refresh.setColorSchemeColors(
            ResourcesCompat.getColor(resources, R.color.colorAccent, null),
            ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        outfit_list__swipe_refresh.setOnRefreshListener {
            outfitsListViewModel.getOutfitsFromApi().observe(viewLifecycleOwner, outfitsListObserver)
        }
    }

    private fun setupRecyclerView() {
        adapter = OutfitAdapter(ArrayList())
        adapter.setListener(this)
        adapter.setHasStableIds(true)
        outfit_list__recycler_view.adapter = adapter
    }

    private fun setLoadingIndicator(active: Boolean) {
        if (view == null) {
            return
        }
        val srl: SwipeRefreshLayout = requireView().findViewById(R.id.outfit_list__swipe_refresh)
        srl.post { srl.isRefreshing = active }
    }

    private fun showSnackbar(resId: Int) {
        Snackbar.make(outfit_list__recycler_view, resId, Snackbar.LENGTH_SHORT).show()
    }

    override fun onClickOutfit(id: Int, position: Int) {
        val detailIntent = Intent(context, OutfitDetailActivity::class.java)
        detailIntent.putExtra(EXTRA_ID, id)
        detailIntent.putExtra(EXTRA_POS, position)
        startActivityForResult(detailIntent, DETAILS)
    }

    override fun onFavoriteOutfit(id: Int) {
        outfitsListViewModel.setFavoriteById(id)
    }

    override fun onUnfavoriteOutfit(id: Int) {
        outfitsListViewModel.unsetFavoriteById(id)
    }

    companion object {
        const val DETAILS = 0
        const val CAMERA = 1
    }

}