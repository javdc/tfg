package com.fashionlint.android.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.like.LikeButton
import com.like.OnLikeListener
import com.fashionlint.android.R
import com.fashionlint.android.database.OutfitDomain
import kotlinx.android.synthetic.main.row_outfit.view.*

class OutfitAdapter(outfits: List<OutfitDomain>) : RecyclerView.Adapter<OutfitAdapter.ViewHolder>() {

    private var outfitsData = mutableListOf<OutfitDomain>()
    private var listener: OutfitsListAdapterActions? = null

    init {
        this.outfitsData.addAll(outfits)
    }

    interface OutfitsListAdapterActions {
        fun onClickOutfit(id: Int, position: Int)
        fun onFavoriteOutfit(id: Int)
        fun onUnfavoriteOutfit(id: Int)
    }

    fun setListener(listener: OutfitsListAdapterActions?) {
        this.listener = listener
    }

    fun setOutfits(outfits: List<OutfitDomain>) {
        this.outfitsData = outfits as MutableList<OutfitDomain>
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_outfit,
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = outfitsData[position]
        holder.bindTo(current)
    }

    override fun getItemCount(): Int {
        return outfitsData.size
    }

    override fun getItemId(position: Int): Long {
        return outfitsData[position].id.toLong()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        fun bindTo(outfit: OutfitDomain) {
            setOutfitInfo(outfit)
            setFavoriteIcon(outfit)
            setPhoto(outfit)
        }

        private fun setOutfitInfo(outfit: OutfitDomain) {
            itemView.row_outfit__label__name.text = outfit.name
            itemView.row_outfit__label__creator.text = outfit.source
        }

        private fun setFavoriteIcon(outfit: OutfitDomain) {
            itemView.row_outfit__like_btn__favorite.isLiked = outfit.favorite
            itemView.row_outfit__like_btn__favorite.setOnLikeListener(object : OnLikeListener {
                override fun liked(likeButton: LikeButton) {
                    outfitsData[adapterPosition].id.let { listener?.onFavoriteOutfit(it) }
                }
                override fun unLiked(likeButton: LikeButton) {
                    outfitsData[adapterPosition].id.let { listener?.onUnfavoriteOutfit(it) }
                }
            })
        }

        private fun setPhoto(outfit: OutfitDomain) {
            outfit.images?.getOrNull(0)?.let { image ->
                Glide.with(itemView.context)
                    .load(image)
                    .fitCenter()
                    .into(itemView.row_outfit__img__photo)
            }
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            val id = outfitsData[position].id
            listener?.onClickOutfit(id, position)
        }

    }

}