package com.fashionlint.android.api

import com.google.gson.annotations.SerializedName

data class ImageUploadResponseDto(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("filename")
    val filename: String
)