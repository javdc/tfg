package com.fashionlint.android.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "outfits")
data class OutfitDomain(
    @PrimaryKey(autoGenerate = true) var id: Int,
    val name: String,
    val brand: String?,
    val source: String?,
    val price: Float?,
    val currency: String?,
    val description: String?,
    val style: String?,
    var images: List<String>?,
    val link: String?,
    var favorite: Boolean
)