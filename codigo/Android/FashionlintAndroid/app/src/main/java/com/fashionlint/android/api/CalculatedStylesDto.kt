package com.fashionlint.android.api

import com.google.gson.annotations.SerializedName

data class CalculatedStylesDto(
    @SerializedName("casual")
    val casual: Double,
    @SerializedName("formal")
    val formal: Double,
    @SerializedName("sport")
    val sport: Double
)