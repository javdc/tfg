package com.fashionlint.android.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.preference.PreferenceManager
import com.fashionlint.android.R
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private var sharedPreferences: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(main_toolbar)

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(baseContext)
        if (isFirstRun()) {
            startWelcomeActivity()
        } else if (!isLoggedIn()) {
            startLoginActivity()
        } else {
            setupNavigationDrawer()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun setupNavigationDrawer() {
        val name = sharedPreferences?.getString(LoginActivity.NAME_KEY, "anonymous")
        val loggedAsLabel = nav_view.getHeaderView(0)
            .findViewById<TextView>(R.id.nav_header__text__logged_in_as)
        loggedAsLabel?.text = getString(R.string.general__logged_in_as, name)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(setOf(
            R.id.nav_outfits,
            R.id.nav_favorite_outfits,
            R.id.nav_my_outfits
        ), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        val settingsMenuItem = nav_view.menu.findItem(R.id.drawer__label__settings)
        settingsMenuItem.setOnMenuItemClickListener { startSettingsActivity() }

        val logoutMenuItem = nav_view.menu.findItem(R.id.drawer__label__logout)
        logoutMenuItem.setOnMenuItemClickListener { logout() }
    }

    private fun logout(): Boolean {
        sharedPreferences?.edit()?.apply {
            remove(LoginActivity.USERNAME_KEY)
            remove(LoginActivity.NAME_KEY)
            remove(LoginActivity.TOKEN_KEY)
            apply()
        }
        startLoginActivity()
        return true
    }

    private fun isFirstRun(): Boolean {
        val previouslyStarted = sharedPreferences?.getBoolean("PREVIOUSLY_STARTED", false)
        return if (previouslyStarted == false) {
            val edit = sharedPreferences?.edit()
            edit?.putBoolean("PREVIOUSLY_STARTED", true)
            edit?.apply()
            true
        } else {
            false
        }
    }

    private fun isLoggedIn(): Boolean {
        val accessToken = sharedPreferences?.getString(LoginActivity.TOKEN_KEY, "")
        return !TextUtils.isEmpty(accessToken) // TODO: Comprobar si token es válido
    }

    private fun startLoginActivity() {
        val intent = Intent(baseContext, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun startWelcomeActivity() {
        val intent = Intent(baseContext, WelcomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun startSettingsActivity(): Boolean {
        val intent = Intent(baseContext, SettingsActivity::class.java)
        startActivity(intent)
        return true
    }

}