package com.fashionlint.android.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.preference.PreferenceManager
import com.fashionlint.android.R
import com.fashionlint.android.activity.LoginActivity
import com.fashionlint.android.activity.LoginActivity.Companion.TOKEN_KEY
import com.fashionlint.android.repository.OutfitRepository
import com.fashionlint.android.utils.Resource
import com.fashionlint.android.activity.SettingsActivity.Companion.HOST_KEY
import com.fashionlint.android.database.OutfitDatabase
import com.fashionlint.android.database.OutfitDomain
import kotlinx.coroutines.Job

class OutfitsListViewModel(application: Application) : AndroidViewModel(application) {

    private val outfitRepository: OutfitRepository
    private val job = Job()

    init {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(application)
        val apiBaseUrl = sharedPreferences.getString(
            HOST_KEY,
            application.getString(R.string.settings__default_host)
        ) ?: application.getString(R.string.settings__default_host)
        val token = sharedPreferences.getString(TOKEN_KEY, null)
        val loggedUsername = sharedPreferences.getString(LoginActivity.USERNAME_KEY, null)
        val dao = OutfitDatabase.getInstance(application.applicationContext).outfitDatabaseDao
        outfitRepository = OutfitRepository(apiBaseUrl, "Bearer $token", "$loggedUsername", dao, job)
    }

    override fun onCleared() {
        job.cancel()
        super.onCleared()
    }

    fun getOutfits(): LiveData<Resource<List<OutfitDomain>>> {
        return outfitRepository.getOutfits()
    }

    fun getOutfitsFromApi(): LiveData<Resource<List<OutfitDomain>>> {
        return outfitRepository.getOutfitsFromApi()
    }

    fun getOutfitsOrderedByName(): LiveData<Resource<List<OutfitDomain>>> {
        return outfitRepository.getOrderedByName()
    }

    fun setFavoriteById(id: Int) {
        return outfitRepository.setFavoriteById(id)
    }

    fun unsetFavoriteById(id: Int) {
        return outfitRepository.unsetFavoriteById(id)
    }

}