package com.fashionlint.android.activity

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.fashionlint.android.R
import com.github.appintro.AppIntro2
import com.github.appintro.AppIntroFragment
import com.github.appintro.AppIntroPageTransformerType
import com.github.appintro.model.SliderPage

class WelcomeActivity : AppIntro2() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addSlide(
            AppIntroFragment.newInstance(
            getString(R.string.welcome__title_1),
            getString(R.string.welcome__description_1),
            imageDrawable = R.drawable.welcome_clothes,
            backgroundDrawable = R.drawable.ic_launcher_background
        ))

        addSlide(AppIntroFragment.newInstance(
            getString(R.string.welcome__title_2),
            getString(R.string.welcome__description_2),
            imageDrawable = R.drawable.welcome_fab,
            backgroundDrawable = R.drawable.back_slide2
        ))

        addSlide(AppIntroFragment.newInstance(
            getString(R.string.welcome__title_3),
            getString(R.string.welcome__description_3),
            imageDrawable = R.drawable.welcome_photo,
            backgroundDrawable = R.drawable.back_slide3
        ))

        addSlide(AppIntroFragment.newInstance(
            getString(R.string.welcome__title_4),
            getString(R.string.welcome__description_4),
            imageDrawable = R.mipmap.ic_launcher,
            backgroundDrawable = R.drawable.back_slide4
        ))

        showStatusBar(true)

        setTransformer(AppIntroPageTransformerType.Parallax(
            titleParallaxFactor = -1.0,
            imageParallaxFactor = 2.0,
            descriptionParallaxFactor = -1.0
        ))
    }

    public override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        openMainActivity()
    }

    public override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        openMainActivity()
    }

    private fun openMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}