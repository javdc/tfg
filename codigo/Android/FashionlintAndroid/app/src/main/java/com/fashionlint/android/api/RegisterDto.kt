package com.fashionlint.android.api

import com.google.gson.annotations.SerializedName

data class RegisterDto(
    @SerializedName("username")
    val username: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("password")
    val password: String
)