package com.fashionlint.android.repository

import android.content.ContentResolver
import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fashionlint.android.api.*
import com.fashionlint.android.database.OutfitDatabaseDao
import com.fashionlint.android.database.OutfitDomain
import com.fashionlint.android.utils.ErrorEnum
import com.fashionlint.android.utils.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import retrofit2.awaitResponse
import kotlin.coroutines.CoroutineContext


class OutfitRepository(private val apiBaseUrl: String, private val bearer: String, private val loggedUsername: String, private val dao: OutfitDatabaseDao, private val job: Job): CoroutineScope {

    private val fashionlintWs = RetrofitInstance.retrofit(apiBaseUrl).create(FashionlintWs::class.java)

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO

    fun getOutfitsFromApi(): LiveData<Resource<List<OutfitDomain>>> {
        val liveData = MutableLiveData<Resource<List<OutfitDomain>>>()
        launch {
            liveData.postValue(Resource.Loading(dao.getAll()))
            try {
                fashionlintWs.getOutfits().awaitResponse().let { response ->
                    if (response.isSuccessful) {
                        val outfits = response.body()?.outfits?.map {
                            OutfitDomain(
                                id = it.id,
                                name = it.name,
                                brand = it.brand,
                                source = it.source,
                                price = it.price,
                                currency = it.currency,
                                description = it.description,
                                style = it.style,
                                images = it.image?.let { image -> listOf("$apiBaseUrl/public/$image") } ?: listOf(""),
                                link = it.link,
                                favorite = false
                            )
                        }
                        outfits?.let {
                            val favIds = dao.getFavoriteIds()
                            dao.insert(outfits)
                            for (id in favIds) {
                                dao.setFavoriteById(id)
                            }
                        }
                        liveData.postValue(Resource.Success(dao.getAll()))
                    } else if (response.code() == 404) {
                        liveData.postValue(Resource.Error(ErrorEnum.NO_OUTFITS))
                    } else {
                        liveData.postValue(Resource.Error(ErrorEnum.SERVER_ERROR))
                    }
                }
            } catch (ex: Exception) {
                liveData.postValue(Resource.Error(ErrorEnum.RETROFIT_ERROR))
                Log.e("RETROFIT", "Error connecting to server", ex)
            }
        }
        return liveData
    }

    fun uploadImageToApi(imageUri: Uri, contentResolver: ContentResolver): LiveData<Resource<String>> {
        val liveData = MutableLiveData<Resource<String>>()
        launch {
            liveData.postValue(Resource.Loading())
            try {
                val requestBody = ContentUriRequestBody(contentResolver, imageUri)
                val imagePart = MultipartBody.Part.createFormData("img_file", "image.jpg", requestBody)
                try {
                    fashionlintWs.uploadImage(bearer, imagePart).awaitResponse().let { response ->
                        if (response.isSuccessful) {
                            liveData.postValue(Resource.Success(response.body()?.filename))
                        } else {
                            liveData.postValue(Resource.Error(ErrorEnum.SERVER_ERROR))
                        }
                    }
                } catch (ex: Exception) {
                    liveData.postValue(Resource.Error(ErrorEnum.RETROFIT_ERROR))
                    Log.e("RETROFIT", "Error uploading image", ex)
                }
            } catch (ex: Exception) {
                liveData.postValue(Resource.Error(ErrorEnum.IMAGE_LOADING_ERROR))
            }
        }
        return liveData
    }

    fun postOutfitToApi(outfit: OutfitDto): LiveData<Resource<CalculatedStylesDto>> {
        val liveData = MutableLiveData<Resource<CalculatedStylesDto>>()
        launch {
            liveData.postValue(Resource.Loading())
            try {
                fashionlintWs.postOutfit(bearer, outfit).awaitResponse().let { response ->
                    if (response.isSuccessful) {
                        val calculatedStyles = response.body()?.calculatedStyles
                        liveData.postValue(Resource.Success(calculatedStyles))
                    } else {
                        liveData.postValue(Resource.Error(ErrorEnum.SERVER_ERROR))
                    }
                }
            } catch (ex: Exception) {
                liveData.postValue(Resource.Error(ErrorEnum.RETROFIT_ERROR))
                Log.e("RETROFIT", "Error connecting to server", ex)
            }
        }
        return liveData
    }

    fun getOutfits(): LiveData<Resource<List<OutfitDomain>>> {
        val liveData = MutableLiveData<Resource<List<OutfitDomain>>>()
        launch {
            val outfits = dao.getAll()
            if (outfits.isNotEmpty()) {
                liveData.postValue(Resource.Success(outfits))
            } else {
                liveData.postValue(Resource.Error(ErrorEnum.NO_OUTFITS))
            }
        }
        return liveData
    }

    fun getOrderedByName(): LiveData<Resource<List<OutfitDomain>>> {
        val liveData = MutableLiveData<Resource<List<OutfitDomain>>>()
        launch {
            val outfits = dao.getAllOrderByName()
            if (outfits.isNotEmpty()) {
                liveData.postValue(Resource.Success(outfits))
            } else {
                liveData.postValue(Resource.Error(ErrorEnum.NO_OUTFITS))
            }
        }
        return liveData
    }

    fun getFavorites(): LiveData<Resource<List<OutfitDomain>>> {
        val liveData = MutableLiveData<Resource<List<OutfitDomain>>>()
        launch {
            val outfits = dao.getFavorites()
            if (outfits.isNotEmpty()) {
                liveData.postValue(Resource.Success(outfits))
            } else {
                liveData.postValue(Resource.Error(ErrorEnum.NO_OUTFITS))
            }
        }
        return liveData
    }

    fun getMyOutfits(): LiveData<Resource<List<OutfitDomain>>> {
        val liveData = MutableLiveData<Resource<List<OutfitDomain>>>()
        launch {
            val outfits = dao.getFromUser(loggedUsername)
            if (outfits.isNotEmpty()) {
                liveData.postValue(Resource.Success(outfits))
            } else {
                liveData.postValue(Resource.Error(ErrorEnum.NO_OUTFITS))
            }
        }
        return liveData
    }

    fun getOutfit(id: Int): LiveData<OutfitDomain?> {
        val liveData = MutableLiveData<OutfitDomain?>()
        launch {
            liveData.postValue(dao.get(id))
        }
        return liveData
    }

    fun insertOutfit(outfit: OutfitDomain) {
        launch {
            dao.insert(outfit)
        }
    }

    fun setFavoriteById(id: Int) {
        launch {
            dao.setFavoriteById(id)
        }
    }

    fun unsetFavoriteById(id: Int) {
        launch {
            dao.unsetFavoriteById(id)
        }
    }

    fun getOutfitsByStyle(style: String): LiveData<Resource<List<OutfitDomain>>> {
        val liveData = MutableLiveData<Resource<List<OutfitDomain>>>()
        launch {
            liveData.postValue(Resource.Success(dao.getByStyle(style)))
        }
        return liveData
    }

}