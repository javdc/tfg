package com.fashionlint.android.api

import com.google.gson.annotations.SerializedName

data class OutfitDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("brand")
    val brand: String?,
    @SerializedName("source")
    val source: String?,
    @SerializedName("price")
    val price: Float?,
    @SerializedName("currency")
    val currency: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("style")
    val style: String?,
    @SerializedName("image")
    val image: String?,
    @SerializedName("link")
    val link: String?
)