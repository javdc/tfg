import numpy as np
import os
import cv2

DIR_TRAIN = 'data/train'
DIR_TEST = 'data/test'
CATEGORIES = ['prenda', 'modelo']
IMG_SIZE = 28

X_train = []
y_train = []
X_test = []
y_test = []
test_data = []
training_data = []


def create_training_data():
    for category in CATEGORIES:
        path = os.path.join(DIR_TRAIN, category)
        class_num = CATEGORIES.index(category)
        for img in os.listdir(path):
            try:
                img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
                new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
                #print(new_array)
                training_data.append([new_array, class_num])
            except Exception as e:
                pass


def create_test_data():
    for category in CATEGORIES:
        path = os.path.join(DIR_TEST, category)
        class_num = CATEGORIES.index(category)
        for img in os.listdir(path):
            try:
                img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
                new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
                test_data.append([new_array, class_num])
            except Exception as e:
                pass


create_training_data()
create_test_data()

for features, label in training_data:
    X_train.append(features)
    y_train.append(label)

X_train = np.array(X_train)
y_train = np.array(y_train)


for features, label in test_data:
    X_test.append(features)
    y_test.append(label)

X_test = np.array(X_test)
y_test = np.array(y_test)

#Get the shape of x_train
print('x_train shape:', X_train.shape)

#Get the shape of y_train
print('y_train shape:', y_train.shape)

#Get the shape of x_train
print('x_test shape:', X_test.shape)

#Get the shape of y_train
print('y_test shape:', y_test.shape)

np.save('./npy/modelo-prenda/X_test.npy', X_test)
np.save('./npy/modelo-prenda/y_test.npy', y_test)
np.save('./npy/modelo-prenda/X_train.npy', X_train)
np.save('./npy/modelo-prenda/y_train.npy', y_train)