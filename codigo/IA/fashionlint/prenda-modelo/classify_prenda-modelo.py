import os
import shutil

import cv2
import keras
import numpy as np

model = keras.models.load_model('./models/prenda_modelo.h5')

X_test = np.load('./npy/modelo-prenda/X_test.npy')
y_test = np.load('./npy/modelo-prenda/y_test.npy')

loss, acc = model.evaluate(X_test, y_test, verbose=2)
print("Restored model, accuracy: {:5.2f}%".format(100 * acc))

PATH = 'data/main'
IMG_SIZE = 28

print(X_test.shape)


def classify():
    i = 0
    for img in os.listdir(PATH):
        img_directory = os.path.join(PATH, img)
        try:
            img_array = cv2.imread(img_directory, cv2.IMREAD_GRAYSCALE)
            new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
            image = np.array(new_array)
            image = np.expand_dims(image, axis=0)  # change shape from (8,) to (1,8)
            prediction = model.predict(image)
            if prediction[0][0] < prediction[0][1]:
                # mueve modelo
                shutil.move(img_directory, os.path.join(PATH, 'modelo', img))
            else:
                shutil.move(img_directory, os.path.join(PATH, 'prenda', img))
        except Exception as e:
            print('error')
            pass


classify()
