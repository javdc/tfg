import numpy as np
import tensorflow as tf

DIR_TRAIN = 'data/train'
DIR_TEST = 'data/test'
CATEGORIES = ['prenda', 'modelo']
IMG_SIZE = 32

X_test = np.load('./npy/modelo-prenda/X_test.npy')
y_test = np.load('./npy/modelo-prenda/y_test.npy')

X_train = np.load('./npy/modelo-prenda/X_train.npy')
y_train = np.load('./npy/modelo-prenda/y_train.npy')

print(X_train.shape)
print(X_test.shape)

model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(2, activation=tf.nn.softmax))

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
model.fit(X_train, y_train, epochs=12)

loss, acc = model.evaluate(X_test, y_test, verbose=2)
print("Restored model, accuracy: {:5.2f}%".format(100 * acc))

model.save('./models/prenda_modelo.h5')