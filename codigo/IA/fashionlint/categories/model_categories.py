import tensorflow as tf
from keras import callbacks
from keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.models import Sequential

print(tf.__version__)
datagen = ImageDataGenerator(rescale=1. / 255, zoom_range=0.2, shear_range=0.2)
earlystopping = callbacks.EarlyStopping(monitor="val_loss",
                                        mode="min", patience=4,
                                        restore_best_weights=True)

resol = (128, 128)
...
# load and iterate training dataset
train_it = datagen.flow_from_directory('./data/train/', target_size=resol, class_mode='categorical', batch_size=30)
# load and iterate validation dataset
val_it = datagen.flow_from_directory('./data/validation/', target_size=resol, class_mode='categorical', batch_size=30)
# load and iterate test dataset
test_it = datagen.flow_from_directory('./data/test/', target_size=resol, class_mode='categorical', batch_size=30)

# confirm the iterator works
batchX, batchy = train_it.next()
print('Batch shape=%s, min=%.3f, max=%.3f' % (batchX.shape, batchX.min(), batchX.max()))

num_classes = 3
epochs = 32

model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=(128, 128, 3)))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam',
              metrics=['accuracy'])

model.fit(train_it, steps_per_epoch=208, validation_data=val_it, validation_steps=16, epochs=epochs,
          use_multiprocessing=True, workers=6, callbacks=[earlystopping])

#evaluate model
loss = model.evaluate(test_it, steps=24)

model.save('../models/category-model-test.h5')
print('succesfully saved')
