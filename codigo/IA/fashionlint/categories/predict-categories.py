import os
import cv2
import numpy as np
import tensorflow.keras as keras
from PIL import Image
from keras.preprocessing.image import ImageDataGenerator
from matplotlib import pyplot as plt


# Validating model accuracy
def validate_model(model, resol):
    datagen = ImageDataGenerator(rescale=1. / 255, zoom_range=0.2, shear_range=0.2)
    test_it = datagen.flow_from_directory('./data/test/', target_size=resol, class_mode='categorical', batch_size=30)
    loss, acc = model.evaluate(test_it, steps=24, verbose=2)
    print("Restored model, accuracy: {:5.2f}%".format(100 * acc))
    batchX, batchy = test_it.next()
    print('Batch shape=%s, min=%.3f, max=%.3f' % (batchX.shape, batchX.min(), batchX.max()))
    label_map = test_it.class_indices
    print(label_map)


def image_preprocessing(dir, im_name):
    # Accessing throw os to script in order to crop the image background
    command = 'cd ~/image-background-remove-tool && python3 main.py -i ' + os.path.join(dir, im_name) + ' -o ' + \
              os.path.join(dir, 'new.png') + ' -m u2net -prep None -postp No'
    os.system(command)

    # Result of cropping has .png extension, fulfilling background with white and conversion to .jpg
    im = Image.open(os.path.join(dir, 'new.png'))
    fill_color = (230, 230, 230)
    im = im.convert("RGBA")
    if im.mode in ('RGBA', 'LA'):
        background = Image.new(im.mode[:-1], im.size, fill_color)
        background.paste(im, im.split()[-1])  # omit transparency
        im = background
    im.convert("RGB").save(os.path.join(dir, 'blank.jpg'))

    # Image size preprocessing and conversion to nparray
    img = cv2.imread(os.path.join(dir, 'blank.jpg'), cv2.IMREAD_COLOR)
    img = cv2.cvtColor(img, cv2.COLOR_BGRA2RGB)
    img = cv2.resize(img, resol)
    img = np.array(img)
    img = np.expand_dims(img, axis=0)
    img = img.astype('float32')
    img /= 255.0

    os.remove(os.path.join(dir, 'new.png'))

    return img


def show_im(img):
    plt.imshow(img, interpolation='nearest')
    plt.show()


def predict(img):
    # {'casual': 0, 'formal': 1, 'sport': 2}
    prediction = model.predict(img, verbose=0)[0]
    # y_classes = prediction.argmax(axis=-1)
    # print(prediction)
    # print(y_classes)
    return dict(zip(['casual', 'formal', 'sport'], prediction))


if __name__ == '__main__':
    dir = os.path.join(os.getcwd(), 'data', 'own')
    im_name = 'test.jpg'
    resol = (128, 128)

    # Loading pre-trained model
    model = keras.models.load_model('./models/category-model.h5')

    validate_model(model, resol)

    im = image_preprocessing(dir, im_name)

    print(predict(im))
