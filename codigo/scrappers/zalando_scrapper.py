from bs4 import BeautifulSoup

import os
import requests
import dns_cache
# import requests_cache

def main():

    print("""
 ____     _              _        ___                                 
|_  /__ _| |__ _ _ _  __| |___   / __| __ _ _ __ _ _ __ _ __  ___ _ _  v0.1
 / // _` | / _` | ' \/ _` / _ \  \__ \/ _| '_/ _` | '_ \ '_ \/ -_) '_|
/___\__,_|_\__,_|_||_\__,_\___/  /___/\__|_| \__,_| .__/ .__/\___|_|  
                                                  |_|  |_|            \n""")

    dns_cache.override_system_resolver()
    # requests_cache.install_cache('zalando_cache')

    user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36"
    headers = {'User-Agent': user_agent}

    count = 0

    category = input("Introduce una categoría (p. ej. ropa-hombre): ")

    pathname = os.path.join(os.getcwd(), "Zalando", category)
    if not os.path.isdir(pathname):
        os.makedirs(pathname)

    try:
        r  = requests.get(f"https://www.zalando.es/{category}/?p=99999999", headers=headers)
        if r.status_code == 200:
            last_page = int(r.url[r.url.index("?p=")+3:])
        else:
            last_page = 200
        print("\nDescargando imágenes. Esto puede tomar un tiempo...")
    except:
        print("\nError de conexión.")
        return

    for page_num in range(1, last_page+1):
        url = f"https://www.zalando.es/{category}/?p={page_num}"
        r  = requests.get(url, headers=headers)
        if r.status_code == 200:
            soup = BeautifulSoup(r.text, features="lxml")
            for link in soup.find_all("img", attrs={"loading" : "lazy"}): 
                img_url = link.get("src")
                img_url_orig = img_url[:img_url.index("?imwidth=")]
                r = requests.get(img_url_orig, stream=True)
                if r.status_code == 200:
                    with open(os.path.join(os.getcwd(), "Zalando", category, f"{count}.jpg"), 'wb') as f:
                        for chunk in r.iter_content(1024):
                            f.write(chunk)
                count += 1
                print(f"\r{count} imágenes descargadas. Página {page_num} de {last_page}.", end="", flush=True)
        else:
            print("Error del servidor.")
            return
    
    print("\nDescarga completada.")

if __name__ == "__main__":
    main()