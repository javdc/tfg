from app import db, bcrypt

# Alias common DB names
Column = db.Column
Model = db.Model
Relationship = db.relationship


class Outfit(Model):
    """ Outfit model for storing outfit related data """

    id = Column(db.Integer, primary_key=True)
    name = Column(db.String(256))
    brand = Column(db.String(128))
    user_id = Column(db.Integer, db.ForeignKey("user.id"))
    source = Column(db.String(128))
    price = Column(db.Integer)
    currency = Column(db.String(3))
    description = Column(db.String(16384))
    style = Column(db.String(128))
    image = Column(db.String(1024))
    link = Column(db.String(1024))

    user = Relationship("User")

    def __init__(self, **kwargs):
        super(Outfit, self).__init__(**kwargs)

    def __repr__(self):
        return f"<Outfit {self.id} - {self.name}>"
