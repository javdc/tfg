# Model Schemas
from app import ma

from .user import User
from .outfit import Outfit


class UserSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = ("name", "username", "outfits")

class OutfitSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = ("id", "name", "brand", "source", "price", "currency", "description", "style", "image", "link")
