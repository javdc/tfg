# Validations with Marshmallow
from marshmallow import Schema, fields
from marshmallow.validate import Regexp, Length


class LoginSchema(Schema):
    """ /auth/login [POST]

    Parameters:
    - Username
    - Password (Str)
    """

    username = fields.Str(required=True, validate=[Length(max=32)])
    password = fields.Str(required=True, validate=[Length(min=4, max=128)])


class RegisterSchema(Schema):
    """ /auth/register [POST]

    Parameters:
    - Username (Str)
    - Name (Str)
    - Password (Str)
    """

    username = fields.Str(
        required=True,
        validate=[
            Length(min=4, max=32),
            Regexp(
                r"^([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_]))?)$",
                error="Invalid username!",
            ),
        ],
    )
    name = fields.Str(required=True, validate=[Length(max=128)])
    password = fields.Str(required=True, validate=[Length(min=4, max=128)])
