from flask_restx import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity

from .service import UserService
from .dto import UserDto

api = UserDto.api
data_resp = UserDto.data_resp


@api.route("/<string:username>")
class UserGet(Resource):
    @api.response(200, "User data successfully sent", data_resp)
    @api.response(404, "User not found!")
    @api.doc(security="accessToken")
    @jwt_required
    def get(self, username):
        """ Get a specific user's data by their username """
        return UserService.get_user_data_by_username(username)


@api.route("/get_current_user")
class CurrentUser(Resource):
    @api.doc(security="accessToken")
    @jwt_required
    def get(self):
        """ Get user data by access token """
        id = get_jwt_identity()
        return UserService.get_user_data_by_id(id)