def load_data(outfit_db_obj):
    """ Load outfit's data

    Parameters:
    - Outfit db object
    """
    from app.models.schemas import OutfitSchema

    outfit_schema = OutfitSchema()

    data = outfit_schema.dump(outfit_db_obj)

    return data
