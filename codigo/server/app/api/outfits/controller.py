from flask import request
from flask_restx import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity

from .service import OutfitService
from .dto import OutfitDto
from app import parsers

api = OutfitDto.api
outfit = OutfitDto.outfit
new_outfit = OutfitDto.new_outfit
data_resp = OutfitDto.data_resp
img_resp = OutfitDto.img_resp
list_resp = OutfitDto.list_resp



@api.route("")
class OutfitOperations(Resource):

    @api.response(200, "Outfits sent successfully", img_resp)
    @api.response(404, "There are no outfits")
    def get(self):
        """ Get all outfits in the system """
        return OutfitService.get_all_outfits()
    
    @api.response(201, "Outfit data saved successfully", data_resp)
    @api.response(400, "Malformed data or validations failed.")
    @api.expect(new_outfit, validate=True)
    @api.doc(security="accessToken")
    @jwt_required
    def post(self):
        """ Create a new outfit with given data """
        outfit_data = request.get_json()
        user_id = get_jwt_identity()
        return OutfitService.create_outfit(outfit_data, user_id)


@api.route("/<int:id>")
class OutfitOperationsById(Resource):

    @api.response(200, "Outfit data successfully sent", data_resp)
    @api.response(404, "Outfit not found")
    def get(self, id):
        """ Get a specific outfit by its id """
        return OutfitService.get_outfit_data(id)


@api.route('/image_upload')
class ImageUpload(Resource):

    @api.response(201, "Image uploaded successfully", data_resp)
    @api.response(400, "Malformed data")
    @api.expect(parsers.file_upload)
    @api.doc(security="accessToken")
    @jwt_required
    def post(self):
        """ Upload an image to the server """
        data = parsers.file_upload.parse_args()
        return OutfitService.upload_image(data)


@api.route('/populate_from_zalando/<string:category>/<string:style>/<int:numpages>')
class Populate(Resource):
    @api.doc(params={
        'category': 'Zalando category to populate from',
        'style': 'Style of the populated outfits',
        'numpages': 'Number of pages to parse'
        })
    @api.response(201, "Populated correctly", data_resp)
    @api.response(503, "Couldn't connect to Zalando")
    @api.doc(security="accessToken")
    @jwt_required
    def get(self, category, style, numpages):
        """ Populate database """
        return OutfitService.populate_zalando(category, style, numpages)