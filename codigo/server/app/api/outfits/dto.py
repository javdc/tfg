from flask_restx import Namespace, fields


class OutfitDto:

    api = Namespace("outfits", description="Outfit related operations.")
    
    outfit = api.model(
        "Outfit object",
        {
            "id": fields.Integer,
            "name": fields.String,
            "brand": fields.String,
            "user": fields.Integer,
            "source": fields.String,
            "price": fields.Integer,
            "currency": fields.String,
            "description": fields.String,
            "style": fields.String,
            "image": fields.String,
            "link": fields.String
        },
    )

    new_outfit = api.model(
        "Outfit object to create",
        {
            "name": fields.String,
            "brand": fields.String,
            "price": fields.Integer,
            "currency": fields.String,
            "description": fields.String,
            "image": fields.String,
            "link": fields.String
        },
    )

    data_resp = api.model(
        "Outfit Data Response",
        {
            "status": fields.Boolean,
            "message": fields.String,
            "outfit": fields.Nested(outfit),
        },
    )

    img_resp = api.model(
        "Image Upload Response",
        {
            "status": fields.Boolean,
            "message": fields.String,
            "filename": fields.String,
        },
    )

    list_resp = api.model(
        "Outfits Response",
        {
            "status": fields.Boolean,
            "message": fields.String,
            "outfits": fields.List(fields.Nested(outfit)),
        },
    )

