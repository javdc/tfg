from keras.models import load_model
from skimage.transform import resize
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.compat.v1.keras.backend import set_session
import numpy as np
import os

print("Loading model")
sess = tf.compat.v1.Session()
set_session(sess)
model = load_model(os.path.join(os.getcwd(), "app", "ai-model", "category-model.h5"))
model_classify = load_model(os.path.join(os.getcwd(), "app", "ai-model", "prenda-modelo.h5"))
graph = tf.compat.v1.get_default_graph()