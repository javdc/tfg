from flask import current_app

from app import db
from app.utils import message, err_resp, internal_err_resp
from app.models.outfit import Outfit
from app.models.user import User
from app.models.schemas import OutfitSchema

import os
import uuid

from bs4 import BeautifulSoup
import requests
import dns_cache
import concurrent.futures
from PIL import Image
import cv2
import numpy as np
from .my_model import model, model_classify
import operator

outfit_schema = OutfitSchema()


class OutfitService:
    @staticmethod
    def get_outfit_data(id):
        """ Get outfit data by id """
        if not (outfit := Outfit.query.filter_by(id=id).first()):
            return err_resp("Outfit not found", "outfit_404", 404)

        from .utils import load_data

        try:
            outfit_data = load_data(outfit)

            resp = message(True, "Outfit data sent")
            resp["outfit"] = outfit_data
            return resp, 200

        except Exception as error:
            current_app.logger.error(error)
            return internal_err_resp()
    
    @staticmethod
    def get_all_outfits():
        """ Get all outfits """
        if not (outfits := Outfit.query.all()):
            return err_resp("There are no outfits", "outfits_404", 404)

        from .utils import load_data

        try:
            outfits_data = [load_data(outfit) for outfit in outfits]

            resp = message(True, "Outfits sent")
            resp["outfits"] = outfits_data
            return resp, 200

        except Exception as error:
            current_app.logger.error(error)
            return internal_err_resp()

    @staticmethod
    def create_outfit(data, user_id):
        # Assign vars

        ## Required
        name = data["name"]

        ## Optional
        brand = data.get("brand")
        user = User.query.filter_by(id=user_id).first()
        source = user.username
        price = data.get("price")
        currency = data.get("currency")
        description = data.get("description")
        image = data.get("image")
        prediction = predict(os.path.join( os.getcwd(), 'app', 'public'), image)
        style = max(prediction.items(), key=operator.itemgetter(1))[0]
        link = data.get("link")

        try:
            new_outfit = Outfit(
                name=name,
                brand=brand,
                user=user,
                source=source,
                price=price,
                currency=currency,
                description=description,
                style=style,
                image=image,
                link=link,
            )

            db.session.add(new_outfit)
            db.session.flush()

            # Load the new outfit's info
            outfit_info = outfit_schema.dump(new_outfit)

            # Commit changes to DB
            db.session.commit()

            resp = message(True, "Outfit has been created.")
            resp["outfit"] = outfit_info
            resp["calculated_styles"] = prediction

            return resp, 201

        except Exception as error:
            current_app.logger.error(error)
            return internal_err_resp()

    @staticmethod
    def upload_image(data):
        try:
            destination = os.path.join(current_app.root_path, 'public')
            if not os.path.exists(destination):
                os.makedirs(destination)
            filename = f"{uuid.uuid4()}.jpg"


            img_file = os.path.join(destination, filename)
            data['img_file'].save(img_file)
            #image_preprocessing( os.path.join( os.getcwd(), 'app', 'public'), filename)
            resp = message(True, "Image uploaded")
            resp["filename"] = filename

            return resp, 201
        except Exception as error:
            current_app.logger.error(error)
            return internal_err_resp()
    
    @staticmethod
    def populate_zalando(category, style, pages):
        
        dns_cache.override_system_resolver()
        headers = {'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36"}

        pathname = os.path.join(current_app.root_path, 'public')
        if not os.path.isdir(pathname):
            os.makedirs(pathname)
        
        try: # Try connection to Zalando
            r  = requests.get(f"https://www.zalando.es/{category}/?p=1", headers=headers)
            if r.status_code != 200:
                return err_resp("Error connecting to Zalando", "connection_error", 503)
        
        except Exception as e:
            print(e)
            return err_resp("Error connecting to Zalando", "connection_error", 503)

        for page_num in range(1, pages+1):
            print(f"Parsing page {page_num}")
            parse_zalando_page(f"https://www.zalando.es/{category}/?p={page_num}", category, style, headers)

        resp = message(True, "Populated correctly")
        return resp, 201


def parse_zalando_page(url, category, style, headers):
    r  = requests.get(url, headers=headers)
    if r.status_code == 200:
        outfits = []
        soup = BeautifulSoup(r.text, features="lxml")
        try:
            for a in soup.find_all("a", "cat_infoDetail--ePcG"):
                try:
                    detail_url = "https://www.zalando.es" + a.get("href")
                    r = requests.get(detail_url, headers=headers)
                    soup = BeautifulSoup(r.text, features="lxml")

                    name = soup.find("h1").text

                    try:
                        brand = soup.find("div", {"class": "eDKeQg"}).text
                    except Exception:
                        brand = None

                    try:
                        price = float(soup.find("meta", {"name": "twitter:data1"}).get("content").replace(" €", "").replace(",", "."))
                    except Exception:
                        price = None

                    try:
                        material_divs = soup.find("div", {"data-testid": "pdp-attr_material_care"}).find_all("div")
                        characteristics_divs = soup.find("div", {"data-testid": "pdp-attr_details"}).find_all("div")
                        size_divs = soup.find("div", {"data-testid": "pdp-attr_size_fit"}).find_all("div")
                        description_divs = material_divs + characteristics_divs + size_divs
                        description = ""
                        for div in description_divs:
                            description += div.text + "\n"
                    except Exception:
                        description = None

                    try:
                        img_div = soup.find("div", attrs={"bus" : "[object Object]"})
                        img_src = img_div.find_all("li")[1].find("img").get("src")
                        img_url = img_src[:img_src.index("?imwidth=")]
                        image = save_image(img_url)
                    except Exception:
                        image = False

                    if image is not False:
                        outfit = Outfit(
                            name=name,
                            brand=brand,
                            source="Zalando",
                            price=price,
                            currency="EUR",
                            description=description,
                            style=style,
                            image=image,
                            link=detail_url,
                        )
                        
                        print(f"Created outfit {outfit.name}")
                        db.session.add(outfit)

                except Exception as e:
                    print(e)
        except Exception:
            pass
        finally:
            print("Commiting changes to DB")
            db.session.commit()
    else:
        pass

def save_image(img_url):
    r = requests.get(img_url, stream=True)
    img_filename = f"{uuid.uuid4()}.jpg"
    img_directory = os.path.join(current_app.root_path, 'public', f"{img_filename}")
    if r.status_code == 200:
        with open(img_directory, 'wb') as f:
            for chunk in r.iter_content(1024):
                f.write(chunk)

        if not isModelo(img_directory):
            os.remove(img_directory)
            return False

        return img_filename
    else:
        raise ConnectionError("Error downloading image")

def image_preprocessing(dir, im_name):
    #Accessing through os to script in order to crop the image background
    command = 'cd ' + os.path.join(os.getcwd(), "image-background-remove-tool") + ' && python3 main.py -i ' + os.path.join(dir, im_name) + ' -o ' + os.path.join(dir, 'new.png') + ' -m u2net -prep None -postp No'
    os.system(command)

    # Result of cropping has .png extension, fulfilling background with white and conversion to .jpg
    im = Image.open(os.path.join(dir, 'new.png'))
    fill_color = (220, 220, 220)
    im = im.convert("RGBA")
    if im.mode in ('RGBA', 'LA'):
        background = Image.new(im.mode[:-1], im.size, fill_color)
        background.paste(im, im.split()[-1])  # omit transparency
        im = background
    im.convert("RGB").save(os.path.join(dir, im_name))
    os.remove(os.path.join(dir, 'new.png'))

def predict(dir, im_name):
    # Image size preprocessing and conversion to nparray
    img = cv2.imread(os.path.join(dir, im_name), cv2.IMREAD_COLOR)
    img = cv2.cvtColor(img, cv2.COLOR_BGRA2RGB)
    img = cv2.resize(img, (128, 128))
    img = np.array(img)
    img = np.expand_dims(img, axis=0)
    img = img.astype('float32')
    img /= 255.0
    prediction = model.predict(img)[0]
    prediction = np.float64(prediction)
   
    return dict(zip(['casual', 'formal', 'sport'], prediction))

def isModelo(img_directory):
    img_array = cv2.imread(img_directory, cv2.IMREAD_GRAYSCALE)
    new_array = cv2.resize(img_array, (28, 28))
    image = np.array(new_array)
    image = np.expand_dims(image, axis=0) 
    prediction = model_classify.predict(image)
    if prediction[0][0] < prediction[0][1]:
        #modelo
        return True
    else:
        return False