from flask_restx import Api
from flask import Blueprint

from .auth.controller import api as auth_ns
from .user.controller import api as user_ns
from .outfits.controller import api as outfits_ns

# Import controller APIs as namespaces.
api_bp = Blueprint("api", __name__)

authorizations = {
    'accessToken': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}

api = Api(api_bp, title="Fashionlint API", description="Endpoints to interact with the Fashionlint server API.", authorizations=authorizations)

# API namespaces
api.add_namespace(auth_ns)
api.add_namespace(user_ns)
api.add_namespace(outfits_ns)
