""" Top level module

This module:

- Contains create_app()
- Registers extensions
"""

from flask import Flask
import os
from werkzeug.middleware.shared_data import SharedDataMiddleware
from keras.models import load_model
from skimage.transform import resize
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.compat.v1.keras.backend import set_session
import numpy as np

# Import extensions
from .extensions import bcrypt, cors, db, jwt, ma

# Import config
from config import config_by_name


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])

    register_extensions(app)

    

    # Register blueprints
    from .api import api_bp
    app.register_blueprint(api_bp, url_prefix="/api")

    # Enable static file serving for images
    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
        '/public': os.path.join(app.root_path, 'public')
    })

    return app


def register_extensions(app):
    # Registers flask extensions
    db.init_app(app)
    ma.init_app(app)
    jwt.init_app(app)
    bcrypt.init_app(app)
    cors.init_app(app)
