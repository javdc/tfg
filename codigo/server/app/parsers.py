import werkzeug
from flask_restx import reqparse

file_upload = reqparse.RequestParser()
file_upload.add_argument('img_file',  
                         type=werkzeug.datastructures.FileStorage, 
                         location='files', 
                         required=True, 
                         help='Image file')