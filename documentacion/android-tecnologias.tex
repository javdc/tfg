%!TEX root =  tfg.tex
\chapter{Alternativas para el desarrollo de aplicaciones}

\begin{quotation}[Novelist]{Ernest Hemingway (1899--1961)}
The good parts of a book may be only something a writer is lucky enough to overhear or it may be the wreck of his whole damn life -- and one is as good as the other.
\end{quotation}

\begin{abstract}
	En esta sección se comentarán las distintas opciones existentes para el desarrollo de una aplicación móvil, enfocado concretamente a Android, así como la elección final para el proyecto y las razones detrás de esta decisión.
\end{abstract}

\section{Introducción}

La elección de un buen conjunto de tecnologías es esencial para poder desarrollar una aplicación con la funcionalidad necesaria en un plazo de tiempo razonable y que a su vez cuente con un buen acabado final. En este capítulo se estudiarán las distintas alternativas disponibles para el desarrollo de una aplicación móvil de este calibre, viendo sus principales ventajas e inconvenientes así como su popularidad y uso, para acabar eligiendo un conjunto de tecnologías acorde a nuestros objetivos.

\section{¿Por qué una plataforma móvil?}

Sin duda alguna, los dispositivos móviles han cambiado la forma de conectarnos a internet y de comunicarnos entre nosotros. Aunque en su concepción la principal función de estos dispositivos era la de hacer llamadas, hoy en día eso dista mucho de la realidad, e incluso para muchos ha sustituido al ordenador para todo tipo de tareas, y todo esto en un muy corto periodo de tiempo. Esta explosión de popularidad es debida, en parte, a la facilidad y versatilidad que ofrece un dispositivo móvil: es pequeño, se puede usar en casi cualquier parte y es relativamente económico.

Justamente esa facilidad de uso es lo que buscamos para la aplicación de este proyecto. Supongamos que, para que un usuario pudiese recibir recomendaciones a partir de su vestimenta, tuviese que hacerse una foto, enviarla al ordenador, y buscar entre sus fotos la deseada para poder cargarla en el sistema. Y si no le convenciese, vuelta a empezar. No resulta una experiencia óptima, sino más bien tediosa. Sin embargo, con un móvil solo tendría que buscar su espejo más cercano, abrir la aplicación, hacer la foto y enviarla, todo en un único proceso desde un único dispositivo, haciendo la experiencia mucho más intuitiva y gratificante para el usuario.

\section{Métodos de desarrollo de aplicaciones móviles}

Actualmente existe un gran número de distintas tecnologías para el desarrollo de una aplicación móvil. Cada una de ellas responde a un conjunto de preocupaciones distintas y, a su manera, ofrecen ventajas e inconvenientes según la situación y el resultado final esperado. A grandes rasgos, podemos distinguir entre tres tipos de métodos para el desarrollo de aplicaciones móviles:
\begin{itemize}
	\item Aplicaciones Web Progresivas, o PWA por sus siglas en inglés
	\item Frameworks de desarrollo multiplataforma, como pueden ser Flutter o React Native
	\item Desarrollo de aplicaciones nativo
\end{itemize}

Algo que tienen en común tanto las PWA como los frameworks nombrados es que, a diferencia del desarrollo nativo, por lo general un solo código fuente permite dar soporte a distintas plataformas sin un gran esfuerzo. Esto ha hecho que un considerable número de empresas se planteen este tipo de desarrollo, ya que supondría una reducción de costes considerable al contar únicamente con un equipo de desarrollo para todas las plataformas en vez de un equipo distinto para cada una de ellas. Sin embargo, este tipo de herramientas también pueden traer algunos inconvenientes como mayor dificultad para la conexión con sensores de los dispositivos o una peor experiencia de usuario. A continuación veremos detalladamente cada uno de estos métodos de desarrollo, y cómo se ajustarían a la aplicación que vamos a desarrollar.

\subsection{Aplicaciones Web Progresivas}

A diferencia de las aplicaciones móviles tradicionales, un PWA es, a grandes rasgos, un híbrido de una página web y una aplicación. Este enfoque permite fusionar la comodidad ofrecida por los navegadores web, tales como no tener que descargar la aplicación con antelación para poder usarla, con una apariencia similar a la de una aplicación móvil.

El término Progressive Web App fue acuñado por Alex Russell, ingeniero de Google, en 2015 \cite{pwa}. Como una página web normal y corriente, las Aplicaciones Web Progresivas se alojan en un servidor y se construyen utilizando tecnologías web como HTML5, CSS y JavaScript, se acceden a ellas mediante un enlace y no es necesario enviarlo a las tiendas de aplicaciones como Google Play o App Store. Este tipo de páginas suelen ofrecer su ``instalación'' mediante un acceso directo al escritorio, además de ofrecer notificaciones push mediante el propio navegador.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/android-tecnologias/pwa-homescreen-shortcut.jpg}
	\caption{Ejemplos de PWA dando la posibilidad de instalar un acceso directo en el escritorio del móvil}
	\label{fig:pwaShortcut}
\end{figure}

Los recursos de la aplicación (su código, imágenes y animaciones) se mantienen dentro del navegador, ejecutando un script de "service worker". El service worker es el bloque central de una PWA móvil porque se encarga de todas las tareas en segundo plano, de la sincronización de los contenidos y del correcto funcionamiento en ausencia de una conexión de red estable.

\subsubsection{Ventajas}

Los principales beneficios de las Progressive Web Apps son los siguientes:
\begin{itemize}
	\item Fácil de encontrar y compartir: Como las PWA son accesibles desde un enlace, son bastante fáciles de encontrar a través de cualquier buscador y se pueden compartir a otros usuarios mediante un simple enlace.

	\item Service Workers: Gracias a la API de Service Worker de los navegadores, las PWA pueden ofrecer experiencias similares a una aplicación como almacenamiento en caché, sincronización en segundo plano, notificaciones push y un funcionamiento sin problemas en modo offline independientemente de la plataforma en la que se ejecute.

	\item Mayor compatibilidad y accesibilidad: Al no requerir instalación y encontrarse en un servidor, las PWA suelen tener un tamaño muy bajo. Esto, junto con el hecho de ejecutarse en cualquier navegador, hace que la aplicación sea accesible desde prácticamente cualquier dispositivo, independientemente de su sistema operativo, antiguedad o espacio disponible.
\end{itemize}

\subsubsection{Inconvenientes}

A su vez, las PWA también tiene varios inconvenientes a tener en cuenta:
\begin{itemize}
	\item Acceso limitado a las características del hardware: Las aplicaciones web progresivas no pueden utilizar algunas características hardware de los dispositivos como NFC, Bluetooth, así como sensores tales como el acelerómetro, sensores de proximidad, huellas dactilares y controles avanzados de la cámara, por ejemplo.
	
	\item Peor rendimiento: Al ejecutarse bajo un navegador, las PWA no pueden disfrutar de las ventajas que ofrecen un código nativo compilado y se tienen que limitar a usar JavaScript, algo que en aplicaciones más complejas puede ser crucial para ofrecer una buena experiencia de usuario en dispositivos con menos recursos.

	\item Falta de presencia en las tiendas: Aunque su distribución por enlaces puede parecer una opción ideal, según qué tipo de aplicación se trate los usuarios podrían ser más propensos a buscarlas en una tienda de aplicaciones, lo cual puede hacer perder visibilidad.
\end{itemize}

\subsection{React Native}

Desarrollado y financiado por Facebook, el framework React Native está dirigido a plataformas móviles y se basa en tecnologías web como React y JavaScript. Además, este framework hace uso de código nativo para ofrecer una mayor adaptabilidad y rendimiento en distintas plataformas.

En lugar de utilizar frameworks de IU personalizados, este framework permite tender un ``puente'' e implementar widgets nativos de iOS y Android. En otras palabras, la interfaz de usuario de la aplicación, sus componentes y la lógica de la API escrita en JavaScript se compila directamente a código Kotlin o Java en caso de compilar para Android, y en código Swift u Objective-C en caso de desarrollar para iOS. \cite{reactnativeprinciples}

Además, tanto Facebook como la comunidad React Native son constantes y mantienen el framework actualizado en todo momento.

\subsubsection{Ventajas}

Algunas de las ventajas que podemos encontrar en React Native son las siguientes:

\begin{itemize}
	\item Un gran apoyo de la comunidad: React Native tiene actualmente más de 89000 estrellas en Github. Lanzado en 2015 como un proyecto de código abierto, este framework ha ganado una impresionante popularidad, lo cual significa que muchos han confiado en esta tecnología y es poco probable que sea abandonada, además de contar con una gran cantidad de documentación y ayuda en internet.

	\item Componentes listos para usar: En lugar de escribir todo el código desde cero, React Native proporciona componentes y kits de desarrollo de software (SDK) ya preparados, lo cual ayuda a aumentar notablemente la velocidad de desarrollo de una aplicación.

	\item Sensación de ser nativo: La interfaz, las animaciones y el rendimiento se acercan a una experiencia nativa gracias al uso de elementos nativos de iOS y Android. Según algunos desarrolladores, a veces es difícil distinguir una aplicación nativa de una hecha en este framework.
\end{itemize}

\subsubsection{Inconvenientes}

Para ilustrar las desventajas de este framework, tomamos como ejemplo la experiencia de Airbnb con esta tecnología. Airbnb, una de las mayores empresas dedicadas al alojamiento, vio el potencial de React Native y decidió migrar a esta plataforma. Sin embargo, finalmente no cumplió sus expectativas y se alejó de React Native en favor del desarrollo de aplicaciones nativas.

Los tres principales puntos débiles de React Native que la compañía ha señalado en su blog son: \cite{reactairbnb}

\begin{itemize}
	\item Inmadurez y cambios demasiado rápidos: React Native, al ser más nuevo y ambicioso, tiene cambios constantes que hacen que sea difícil y tedioso aprender a realizar algunas tareas que deberían ser triviales. Esta inmadurez hizo que tuviesen que crear un fork para parchear algunas deficiencias de este entorno.
	
	\item Inconsistencias con el uso en iOS y Android: Las librerías disponibles estaban creadas comúnmente por desarrolladores con experiencia en Android o en iOS, pero no en los dos. Esta comunicación inconsistente entre los componentes nativos y no nativos hacía necesario parchear ciertas partes del código según la plataforma, o incluso tener que utilizar librerías distintas según la plataforma.
	
	\item Peor depuración y resolución de problemas: La experiencia en la recopilación de errores mediante Bugsnag no fue la esperada, la calidad de estos reportes era mucho más baja que en las versiones nativas y la depuración es más difícil si el problema abarcaba código de React Native y nativo, ya que el stacktrace no abarca ambos.
\end{itemize}

\subsection{Flutter}

Flutter es un kit de desarrollo de software (SDK) y un framework de desarrollo de aplicaciones móviles de código abierto. Fue creado en 2018 por Google para construir aplicaciones móviles en el lenguaje Dart, basado en Java y C++. \cite{flutter}

Aunque Flutter no cuenta con una popularidad ni soporte tan grandes como React Native, también compila en código nativo y ofrece muchas herramientas de línea de comandos, APIs, librerías preparadas y widgets. Los principales componentes de este framework son la plataforma Dart, el motor Flutter y la librería Foundation. Los widgets de Flutter usan Material Design y ayudan a crear interfaces atractivas para las plataformas iOS y Android.

Su alto rendimiento, pequeño tamaño, fácil acceso a funciones del dispositivo como almacenamiento, cámara o ubicación, así como el respaldo de Google, hacen de Flutter un framework potente a elegir para el desarrollo de una aplicación móvil multiplataforma.

\subsubsection{Ventajas}

\begin{itemize}
	\item Mayor rapidez en tener una aplicación funcional: Con Flutter no hay necesidad de desarrollar un backend por separado ya que soporta completamente las APIs de Firebase. Además, la comodidad de los widgets disponibles acorta considerablemente el proceso de desarrollo y se vuelve crucial para, por ejemplo, desarrollar un mínimo producto viable (MVP) con toda la funcionalidad necesaria en poco tiempo.
	
	\item Rendimiento nativo: Dart, a diferencia de JavaScript, es un lenguaje que se compila a código nativo, por lo que las aplicaciones desarrolladas en Flutter ofrecen un rendimiento muy similar al de una aplicación completamente nativa.

	\item Funcionalidad de Hot-Reload: Flutter permite ver los cambios en el código de una aplicación instantáneamente, sin tener que recompilarla ni reinstalarla y sin perder el estado en el que se encuentre. Esto puede ahorrar bastante tiempo de desarrollo.
\end{itemize}

\subsubsection{Inconvenientes}

\begin{itemize}
	\item No puede interactuar con las API nativas directamente: Para las características más comunes de la aplicación Flutter ofrece suficiente soporte. Sin embargo, en casos y escenarios más personalizados hace falta escribir código nativo para iOS y Android por separado, así como utilizar diferentes entornos de desarrollo para interactuar con estas API.

	\item Tecnología demasiado nueva: Dart es una tecnología relativamente joven que a veces pierde la batalla con sus rivales más viejos en términos de soporte. Como apareció hace menos de dos años, puede ser más complicado encontrar ayuda para resolver ciertos problemas o librerías que se ajusten a nuestras necesidades.
\end{itemize}

\subsection{Desarrollo nativo}

Una aplicación móvil nativa, a menudo denominada ``aplicación móvil tradicional'', se ajusta completamente a una única plataforma y mantienen su propio código separado por cada plataforma: Swift/Objective-C para iOS, y Kotlin/Java para Android. En términos de rendimiento y experiencia del usuario, las aplicaciones desarrolladas de forma nativa son, por lo general, superiores al resto.

Realmente las ventajas e inconvenientes ya se han visto arriba al comentar el resto de alternativas pero, en resumidas cuentas, son los siguientes:

\subsubsection{Ventajas}

\begin{itemize}
	\item El mejor rendimiento, al ser un lenguaje compilado y estar específicamente diseñado para esa plataforma en concreto.
	\item Mayor soporte, ayuda y documentación, al contar con más años de vida.
	\item Mejor integración con el dispositivo, con acceso de primera categoría a las características del hardware, como la cámara o los sensores.
\end{itemize}

\subsubsection{Inconvenientes}

\begin{itemize}
	\item Necesidad de desarrollar para cada plataforma por separado.
	\item Depender de tiendas de aplicaciones para su distribución masiva.
\end{itemize}

\section{Conclusiones y elección para el proyecto}

Como se ha visto en la sección anterior, cada alternativa ofrece unas importantes ventajas y todas ellas son útiles según el tipo de aplicación que queramos desarrollar. Para nuestro caso concreto, la primera alternativa en ser descartada fueron las Progressive Web Apps, ya que no ofrecen un buen acceso a características de la cámara, estando fuertemente limitadas por el navegador. Por otra parte, tanto Flutter como React Native son unas propuestas bastante interesantes, pero sus principales ventajas no son demasiado útiles para nuestro caso concreto. En el caso de Flutter, la integración con Firebase como backend puede resultar útil para algunos casos, pero no resulta viable cuando el backend requiere de mucho espacio para guardar las prendas y los modelos de machine learning, así como poder hacer scrapping de distintas páginas web haciendo uso de otras librerías externas a esta plataforma. Y, por desgracia, al no disponer de recursos para la prueba de la aplicación en iOS, el soporte multiplataforma toma, en nuestro caso, una menor prioridad.

También conviene mencionar Ionic, otro framework que se ha quedado fuera de la comparación para no hacerla excesivamente larga, pero que desde su aparición en 2013 ha disfrutado cada vez de más popularidad. Este framework permite desarrollar tanto aplicaciones híbridas como Progressive Web Apps usando cualquier framework web para frontend, como Angular, React o Vue \cite{ionic}. Personalmente no le vimos utilidad para este proyecto, ya que es bastante similar en tecnologías a React Native pero sin las ventajas de usar componentes nativos en las vistas.

Esto nos deja con la que es, a nuestro parecer, la mejor opción para el desarrollo de la aplicación de Fashionlint: una aplicación de Android nativa. Esta ofrece el mejor rendimiento posible, un acceso a características como la cámara mucho más avanzado, y la ventaja de poder programar en Kotlin, que toma sus bases en Java y por tanto cuento con más experiencia que JavaScript.