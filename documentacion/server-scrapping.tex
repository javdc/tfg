%!TEX root =  tfg.tex
\chapter{Scrapping}

\begin{quotation}[Philosopher and psychologist]{William James (1842--1910)}
In any project, the most important factor is the belief in success. Without faith, success is impossible.
\end{quotation}

\begin{abstract}
	En este capítulo se detallan las motivaciones que llevaron a querer desarrollar una aplicación para la plataforma Android frente a otras plataformas, y qué ventajas ofrece en nuestro proyecto concreto.
\end{abstract}

\section{Introducción}

El scrapping consiste en la navegación automática por páginas web extrayendo la información deseada. Esto puede resultar útil en muchos ámbitos como por ejemplo en \textit{price mapping}, que es lo que usan la mayoría de plataformas comparadoras de precios. Existe software dedicado explícitamente a esta práctica así como numerosas librerías que facilitan su uso en varios lenguajes de programación.

El objetivo principal del minado de información para Fashionlint es obtener una cantidad masiva de imágenes de moda que sirvieran como entrenamiento para el módulo de inteligencia artificial.


\section{Fuentes de información para los outfits}

En el proceso de scrapping es esencial escoger una fuente de datos que se ajuste lo mejor posible a nuestros objetivos. En el caso de Fashionlint debemos escoger una página que cuente con un gran número de outfits, pero esto no es suficiente. Se debe tener en cuenta que las imágenes extraídas deben pasar por un proceso de aprendizaje automático, por lo que estas deberían ser lo más homogéneas posible entre sí.

La idea al comienzo del proyecto era tener varias fuentes distintas de datos, y que los usuarios solo recibiesen recomendaciones en base a esas fuentes de donde extrayésemos la información. Sin embargo, al buscar distintas tiendas de ropa online, o incluso redes sociales como Instagram o 21buttons, nos topamos con el problema de que estas imágenes no eran homogéneas y contenían mucho ruido: podían tener fondos de todo tipo, en algunas fotos el modelo aparecía sentado, en otros de cuerpo entero, otras imágenes con solo el torso... Por lo que la mayoría de páginas que se consideraron al principio, aunque fuese factible el scrapping en sí, tuvieron que ser descartadas por no contener imágenes homogéneas.

Por suerte, una tienda de ropa online cumplía con todos los requisitos que necesitábamos: Zalando. Esta plataforma internacional de venta de ropa, además de contar con mucha popularidad y, por tanto, una gran variedad de prendas, es también una fuente más que aceptable para poder alimentar el módulo de inteligencia artificial, ya que todas sus fotos tienen un fondo blanco y, por lo general, son de cuerpo entero.

Otra ventaja de Zalando es que toda la ropa está separada en distintas categorías, por lo que el aprendizaje automático puede recibir esta información para poder diferenciar los outfits en diferentes estilos. Estos estilos, tras probar distintas combinaciones, fueron finalmente casual, formal y sport.

\section{Descripción de la implementación}

Por suerte, la página de Zalando no resulta especialmente complicada de parsear. Aunque los nombres de las clases estaban algo ofuscados, con la ayuda del selector de elementos de las herramientas de desarrollador del navegador no fue complicado dar con los elementos de los que necesitábamos obtener la información.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\textwidth]{figures/server-scrapping/developer-tools.png}
	\caption{Uso del selector de elementos de Chromium}
	\label{fig:devTools}
\end{figure}

Con esta información podemos describir a BeautifulSoup qué es lo que necesitamos.
Para la explicación de la implementación se usará un código simplificado.

En primer lugar, es necesario obtener el código html de la página. Para ello, se realiza una petición con la librería \textit{requests}. Al intentar hacer la petición por primera vez, encontramos que \textit{requests} devolvía una página en blanco. Tras investigar sobre el problema, la solución fue configurar un \textit{useragent} de un navegador real a la petición. Esto es debido, seguramente, a una medida de seguridad por parte de Zalando para evitar justamente este tipo de scrapping.

\lstset{language=Python}
\begin{lstlisting}
headers = {'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36"}
r  = requests.get(url, headers=headers)
\end{lstlisting}

Con esta petición, una vez comprobado si el código ha sido correcto, se pasa el contenido a BeautifulSoup de la siguiente manera:

\begin{lstlisting}
soup = BeautifulSoup(r.text, features="lxml")
\end{lstlisting}

Con el código HTML parseado por BeautifulSoup podemos utilizar varios métodos para poder encontrar los elementos que buscamos. Los métodos más útiles son los siguientes:

Para obtener en una lista todos los elementos de tipo ``a'' con la clase ``cat\_infoDetail\-\-ePcG'':

\begin{lstlisting}
soup.find_all("a", "cat_infoDetail--ePcG")
\end{lstlisting}

Para obtener el primer elemento de tipo ``div'' con un el atributo ``bus'' igual a ``[object Object]'':

\begin{lstlisting}
soup.find("div", attrs={"bus" : "[object Object]"})
\end{lstlisting}

Para obtener el valor del atributo ``src'' de un elemento ``img'' guardado en la variable ``image'':

\begin{lstlisting}
image.get("src")
\end{lstlisting}

Como se puede observar, la sintaxis de BeautifulSoup es muy sencilla, pero a la vez potente y más que suficiente para parsear páginas complejas, siempre que no usen JavaScript. Por ejemplo, para obtener el enlace a la imagen de una prenda a partir de su enlace de detalles se usa el siguiente código:

\begin{lstlisting}
detail_url = "https://www.zalando.es" + a.get("href")
r = requests.get(detail_url, headers=headers)
soup = BeautifulSoup(r.text, features="lxml")
div = soup.find("div", attrs={"bus" : "[object Object]"})
img_src = div.find_all("li")[1].find("img").get("src")
img_src_orig = img_src[:img_src.index("?imwidth=")]
r = requests.get(img_src_orig, stream=True)
\end{lstlisting}

Por otra parte, para guardar el archivo, se utilizan la funcionalidad de Python para escribir archivos, usando un buffer de 1024 bytes para evitar problemas de memoria y generando un nombre aleatorio con la librería \textit{uuid}:

\begin{lstlisting}
r = requests.get(img_src_orig, stream=True)
	if r.status_code == 200:
		with open(os.path.join(os.getcwd(), "Zalando", category, f"{str(uuid.uuid4())}.jpg"), 'wb') as f:
			for chunk in r.iter_content(1024):
			f.write(chunk)
\end{lstlisting}