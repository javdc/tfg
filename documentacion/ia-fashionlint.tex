%!TEX root =  tfg.tex
\chapter{Implementación final en Fashionlint}

\begin{quotation}[Pioneer of Compassionate AI Movement]{Amit Ray}
	The coming era of Artificial Intelligence will not be the era of war, but be the era of deep compassion, non-violence, and love.
\end{quotation}

\begin{abstract}
	En este capítulo se muestra la implementación de los módulos de inteligencia artificial, aportando ejemplos de código real y justificaciones al respecto de las decisiones tomadas.
\end{abstract}

\section{Módulo auxiliar}
Como se observa en la planificación, una única red neuronal sería la encargada de categorizar las fotos de los usuarios de Fashionlint. Sin embargo, el equipo se topó con un problema inesperado: ruido en el dataset. Entendemos por ruido cualquier imagen ajena al contexto de entrenamiento y que suponga una bajada en la exactitud final del modelo tras su fase de entrenamiento.

Como se menciona en el capítulo dedicado a la extracción de la información, fue imposible descargar solo y exclusivamente fotografías de modelos de cuerpo entero. Un pequeño porcentaje del dataset final contenía imágenes de prendas sueltas que debían ser eliminadas, entre un 7\% y un 10\% aproximadamente, lo suficiente para tener un impacto negativo notable en el modelo final.

El principal problema residía en que dicha tarea no podía ser realizada de forma manual, debido al tiempo que esto consumiría: abrir directorios con miles de imágenes e ir moviendo una a una a través de la interfaz gráfica del sistema operativo a otra carpeta. Por tanto decidimos clasificar una pequeña parte de las imágenes a mano para entrenar un modelo que pudiera distinguir entre fotografías de cuerpo entero \ref{fig:modelo} y fotografías de prendas sueltas \ref{fig:prenda} como se observa en las figuras referenciadas.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{figures/ia/modelo.jpg}
	\caption{Formato de imagen deseado}
	\label{fig:modelo}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{figures/ia/prenda.jpg}
	\caption{Imagen de prenda suelta a filtrar}
	\label{fig:prenda}
\end{figure}

\subsection{Preprocesamiento de datos}
De largo, el proceso que más tiempo consume en la amplía mayoría de los proyectos de machine learning es el preprocesamiento de los datos de  entrada al algoritmo predictivo, y este no fue la excepción. Cabe destacar que las imágenes son reescaladas a una resolución muy pequeña de 28x28 píxeles y en escala de grises, con el fin de ahorrar tiempo de computo. Esta compresión es posible debido relativa sencillez del problema, ya que nuestra red neuronal no necesita extraer características demasiado complejas, basta con que distinga entre la silueta de un cuerpo humano que es más alargada y esbelta que la de una prenda, la cuál presenta una silueta claramente diferente.
\begin{itemize}
	\item Lo primero que se realizó fue una separación manual de imágenes pertenecientes a las dos categorías deseadas (prenda y modelo). Para ello se tomó la cantidad que se creía prudencial para el éxito del entrenamiento, pero sin sobrecargar el trabajo humano subyacente de está práctica.
	Finalmente se obtuvieron unas 300 muestras de prendas sueltas junto a las 3000 restantes pertenecientes a modelos, que a su vez se dividirían en dos partes: un 90\% de las muestras para el entrenamiento, y un 10\% para la validación.
	Con la estructura del dataset lista según convenio dentro del directorio 'data' \ref{fig:structure}, el paso siguiente consistía en convertir las imágenes a un formato entendible por la red neuronal: arrays de numpy.
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.5\textwidth]{figures/ia/structure.png}
		\caption{Estructura de directorios}
		\label{fig:structure}
	\end{figure}
	\item Para lograr la conversión de las imágenes al formato deseado se declaran dos arrays (de python nativo) vacíos correspondientes a los datos de entrenamiento y los datos de prueba, \textit{trainnign\_data} y \textit{test\_data} respectivamente. Gracias al módulo \textit{os} se itera a lo largo de los distintos directorios leyendo las imágenes y transformándolas por medio del módulo \textit{cv2}, por cada iteración se añade un nuevo elemento a \textit{trainning\_data} o \textit{test\_data} que consta a su vez de dos elementos: la imagen y su categoría.
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.9\textwidth]{figures/ia/testdata.png}
		\caption{Método de ejemplo para poblar \textit{test\_data}}
		\label{fig:testdata}
	\end{figure}
	\item El siguiente paso separa las imágenes tratadas como entrada de nuestra 'función'; 'X' en \textit{X\_test}, y su categoría asociada que de podemos entender como la salida de nuestra función 'Y'; \textit{X\_test}. También se produce la conversión a numpy array. Este proceso se replica con \textit{trainnign\_data}. Llegados a este punto es interesante recordar que todas las estructuras usadas están basadas en \textbf{estándares}, así como la nomenclatura de éstas.
	\begin{figure}[htb]
		\centering
		\includegraphics[width=0.7\textwidth]{figures/ia/xtest.png}
		\caption{Ejemplo con \textit{test\_data}}
		\label{fig:xtest}
	\end{figure}
	\item Por último, y una vez comprobado que el formato es el adecuado se guardan los numpy arrays procesos en formato binario para su posterior uso gracias a la función \textit{save} en el directorio \textit{npy/modelo-prenda} \ref{fig:structure}. De esta manera se externaliza el paso de preprocesamiento del  de entrenamiento, ya que  éste primero resulta muy costoso al iterar sobre numerosos archivos pesados como fotografías, del orden de varios minutos con el equipo y configuración actual.
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.9\textwidth]{figures/ia/save.png}
		\caption{Comprobación y guardado de los datos finalmente procesados }
		\label{fig:save}
	\end{figure}	
\end{itemize}

\subsection{Diseño de la Red, entrenamiento y validación}
En primer lugar se cargan los datos previamente procesados. Para el diseño de la red neuronal se ha optado por una arquitectura sencilla con sólo dos capas ocultas de 128 neuronas cada una y función de activación \textbf{relu}, junto a una capa de entrada que aplana los np arrays multidimensionales de entrada y otra capa de salida respectivamente con función de activación \textbf{softmax} y solo dos neuronas correspondientes a las dos categorías presentes.Finalmente se compila el modelo diseñado con los parámetros recomendados para posteriormente realizar un entrenamiento con la función \textit{fit} durante unas escasas, pero suficientes 12 etapas.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{figures/ia/simplemodel.png}
	\caption{Diseño compilación y entrenamiento de una red neuronal con Keras}
	\label{fig:simplemodel}
\end{figure}
Se evalúa la exactitud del modelo tras su entrenamiento para comprobar que todo ha funcionado de la manera esperada mediante la clase \textit{evaluate} y se guarda para su posterior utilización en el directorio \textbf{/models}. Aunque este ejemplo concreto el tiempo de computo es bajo, es buena práctica guardar los modelos o incluso realizar puntos de guardado durante el entrenamiento, esto será bastante útil en entrenamientos que tienen un tiempo de entrenamiento del orden de horas o incluso días.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{figures/ia/modelsave.png}
	\caption{Comprobación de la exactitud del modelo y posterior guardado}
	\label{fig:modelsave}
\end{figure}
Finalmente se obtiene un modelo capaz de predecir con una exactitud del 94.83\% si una imagen se trata de una prenda o una persona vestida. Mas aún se observa un valor de la función pérdida mínimo (aunque esta no sea una medida canónica si no relativa).
\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{figures/ia/modeleval.png}
	\caption{Comproación de la exatitud del modelo y guardado}
	\label{fig:modeleval}
\end{figure}

\subsection{Automatización}
Una vez que el modelo está entrenado se carga en un nuevo script por medio del método \textit{load} que Keras proporciona. Entonces se puede de manera sencilla iterar sobre el conjunto de imágenes extraídas de zalando aún sin clasificar. Aplicamos las técnica de lectura y preprocesado necesarias para ser evaluadas por el modelo, y se depositan en un directorio u otro dependiendo de la categoría a la que pertenezcan. La categoría la dictamina el método \textit{predict} que aplicamos sobre nuestro modelo cargado y debe ser interpretado de la siquiente manera: el primer valor \textit{prediction[0][0]}	corresponde con la categoría de prendas, mientras que el segundo \textit{prediction[0][1]} a la de modelo. El mayor valor de los dos indica la categoría a la que una fotografía debe pertenecer.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{figures/ia/classify.png}
	\caption{Script para la clasificación de imágenes en distintos directorios basado en modelo predictivo}
	\label{fig:classify}
\end{figure}

\section{Módulo principal}
Al comienzo de la segunda iteración se enfrenta un retraso de un mes respecto a la planificación propuesta. Sin embargo las horas invertidas en el procesamiento de los datos sirvió en gran medida para dar un salto de calidad notable respecto a las expectativas subscritas al modelo. La problemática esta vez se torna mucho más compleja, principalmente debido a la naturaleza del dataset. Recordar que las categorías de ropa extraídas de zalando de manera más o menos confiable son tres: \textbf{casual, formal y sport} \ref{fig:casual}. Los principales impedimentos que podían poner en riesgo el éxito del proyecto fueron los siguientes:
\begin{enumerate}
	\item Poca variedad de muestras. La mayoría de datos de prueba correspondían a la categoría casual. Mientras que la extracción de muestras formales y sport representaba un 30\% del dataset total, la ropa casual ocupaban el 70\% restante. Aún así se contaron con 8000 muestras en total, un número bastante decente según los casos de éxito estudiados.
	\item Características difusas. A veces incluso para un humano se tornaba complicado la tarea de diferenciar entre un look sport y uno casual, no tanto así con la categoría formal que si presentaba características diferenciadoras frente a las otras dos. Ésta quizá fuera la mayor fuente de imprecisión en el modelo, por ello se hace incapié en la calidad del dataset.
	\item Largos tiempo de computación. Hasta entonces no se había experimentado con redes tan complejas y datasets gigalíticos, era difícil intuir si sería un factor limitante ya que depende de muchas variables como el capacidad de la máquina, el nivel de compresión de los datos o las etapas de entrenamiento necesarias para optimizar el resultado entre otros muchos.
	\item Puesta en producción. El entrenamiento con un dataset unificado preocupaba al equipo, ya que con un uso real de la aplicación las características podían hacerse incluso más difusas al agregar fondo a las fotografías (a diferencia del fondo gris unificado de zalando) de los usuarios, condiciones de baja luz, formato  o peso de los archivos como principales exponentes. En otras palabras, el modelo no sería capaz de procesar una fotografía real al solo haber sido entrenado con fotos de modelos con un formato limpio, encuadrado y bien iluminado. La obtención de un dataset de estas características tampoco se planteaba como una opción viable.
\end{enumerate} 
\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{figures/ia/casual.jpg}
	\caption{Ejemplo de modelo vistiendo ropa de estilo \textbf{casual}}
	\label{fig:casual}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{figures/ia/formal.jpg}
	\caption{Ejemplo de modelo vistiendo ropa de estilo \textbf{formal}}
	\label{fig:formal}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{figures/ia/sport.jpg}
	\caption{Ejemplo de modelo vistiendo ropa de estilo \textbf{sport}}
	\label{fig:sport}
\end{figure}

\subsection{Preprocesamiento: ImageDataGenerator}
Para el preprocesamiento de las imágenes y la organización del dataset se esperaba invertir más recursos en que el módulo anterior, pero gracias a la labor de investigación constante se encontró una herramienta de Keras para el preprocesado de imágenes con el nombre de \textit{ImageDataGenerator} perteneciente al módulo de \textit{preprocessing}.
\begin{itemize}
	\item Para la correcta implementación de la herramienta lo primer que se necesita es modelar los directorios de manera estándar. El directorio que contendrá todo nuestro dataset pasará a ser llama \textit{data} y dentro de este se crearán los tres grupos encargados del entrenamiento, las pruebas y la validación: \textit{trainning}, \textit{test} y \textit{validation}. A su vez cada una de las divisiones del dataset, con el porcentaje correspondiente de muestras para cada uno, debe albergar los directorios correspondientes a las categorías que se desean clasificar. En este último nivel la sintaxis si es libre.
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.4\textwidth]{figures/ia/datastructure.png}
		\caption{Estructura estándar del dataset para Fashionlint}
		\label{fig:datastructure}
	\end{figure}
	\item Gracias a esta utilidad podemos segmentar los datos en partes más pequeñas e ir entrenando el modelo paulatinamente, de otra manera se requeriría de un equipo extremadamente potente que pudiera cargar en memoria todo el dataset de una sola vez. Esta división viene definida en el \textit{batch\_size}. Para 16gb de ram del equipo el mejor ajuste en tiempo y rendimiento fue la separación de muestras en tiradas de 30 imágenes.
	
	\item En tres lineas de código se auna el trabajo de redimensionar imágenes, normalizar sus valores, iterar sobre directorios, convertirlas a np array e incluso permite añadir parámetros para realizar \textbf{data augmentation} en el momento de instanciar la clase. Como se ve en el siguiente ejemplo de código, tras instanciar la clase se hace uso del método \textit{flow\_from\_directory} en el que sólo se tiene que especificar la fuente de los datos, el tipo de problema (categórico para el caso) y la resolución deseada de 128 píxeles.
	\begin{figure}[H]
		\centering
		\includegraphics[width=1\textwidth]{figures/ia/imagedata.png}
		\caption{Preprocesamiento del dataset de Fashionlint con \textbf{ImageDataGenerator}}
		\label{fig:imagedate}
	\end{figure}
	\item Finalmente obtenemos que la estructura de un batch es el siguiente array multidimensional: \textit{batch shape (30, 128, 128, 3) }. Se puede leer como, 30 fotografías de 128x128 píxeles de resolución en los 3 planos RGB.
\end{itemize}

\subsection{Características de la red neuronal y pruebas}
Para la red neuronal del módulo principal se ha escogido una arquitectura convolucional. Debido a la popularidad de este tipo de implementación, Keras provee los distintos tipos de capas que simplifican una tarea a priori compleja. Esta red se compone de ocho capas en su totalidad:
\begin{enumerate}
	\item La capa de entrada es de tipo \textbf{Conv2D}. En las capas convolucionales debemos declarar un kernel, éste dictaminará la dimensión del filtro que recorrerá toda la imagen creando como salida el denominado \textbf{mapa de activación}. Para este ejemplo concreto el kernel tiene una dimensión de 3x3 píxeles. En lenguaje natural: en esta capa dividimos la imagen en pequeñas tomas de 3x3 complementarias entre sí para la posterior búsqueda de características comparándolas entre sí, como puede ser el cambio de color de una zona de la imagen a otra o la silueta definida por un objeto. Los elementos del mapa de activación redundantes serán descartadas en futuras capas, como por ejemplo pueden ser las pertenecientes al fondo ya que no aportan información relevante para el aprendizaje. Otros parámetros necesarios son: el número de neuronas(32), la función de activación (relu) y el formato del np array correspondiente entrada de la imagen procesada (128, 128, 3). La siguiente \href{https://www.pyimagesearch.com/2018/12/31/keras-conv2d-and-convolutional-layers/}{imagen} muestra el plano conceptual de esta operación.
	\begin{figure}[H]
		\centering
		\includegraphics[width=1\textwidth]{figures/ia/convfilter.png}
		\caption{Pasos en transformación por filtro convolucional}
		\label{fig:convfilter}
	\end{figure}
	\item Como se menciona anteriormente es necesario filtrar los elementos del mapa de activación para quedarnos con aquellos más relevantes para la detección de características, para ello se añade una capa de max pooling \textbf{MaxPooling2D} con el filtro de 2x2 píxeles, éste suele reducir a la mitad el tamaño de la salida del filtro convolución que era de 3x3 píxeles. El filtrado a través de max pooling es el que mejores resultados arroja en los problemas de 'computer vision' y clasificación de imágenes, frente a otras implementaciones como el average pooling o el global pooling. Esta técnica saca a relucir la o las características más presente en los subdivisiones del mapa de activación, como lineas verticales u horizontales en la imagen. El funcionamiento sería el \href{https://computersciencewiki.org/index.php/Max-pooling_/_Pooling}{siguiente}.
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.6\textwidth]{figures/ia/maxpooling.png}
		\caption{Cálculo de maxpooling en una matriz}
		\label{fig:maxpooling}
	\end{figure}
	\item Ahora se añade una capa de \textbf{Dropout} que tiene como función principal prevenir el \textit{over-fitting}. Como su propia traducción literal indica esta capa es la encargada de 'abandonar' cálculos del entrenamiento con un ratio del 25\% en la activación de las neuronas y por tanto la conexión entre ellas, aunque un término más acertado podría ser 'ignorar'. En el plano teórico, la supresión de estas conexiones regula la co-dependencia entre neuronas lo que se traduce en : reducción el tiempo de entrenamiento por cada etapa, aunque son necesarias más etapas para alcanzar la convergencia. Por otro lado obliga a la red a aprender de manera más robusta ofreciendo una mayor capacidad de abstracción en el resultado final de las predicciones. A cpntinuación se muestra un \href{https://medium.com/@amarbudhiraja/https-medium-com-amarbudhiraja-learning-less-to-learn-better-dropout-in-deep-machine-learning-74334da4bfc5}{ejemplo}.
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.9\textwidth]{figures/ia/dropout.png}
		\caption{Comparación entre el antes y el después de aplicar \textbf{dropout} a una red neuronal}
		\label{fig:dropout}
	\end{figure}
	\item La siguiente capa \textbf{Flatten} se encarga de aplanar la salida de la capa de pooling, preparando los datos para la entrada en una capa densa convencional. Se puede observar su funcionamiento de manera esquemática en esta \href{https://missinglink.ai/guides/keras/using-keras-flatten-operation-cnn-models-code-examples/}{figura}.
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.8\textwidth]{figures/ia/flatten.png}
		\caption{flaten}
		\label{fig:flaten}
	\end{figure}
	\item Se declara una capa \textbf{Dense} con 128 unidades de computo fuertemente conexas, las suficientes para llevar a cabo potentes predicciones pero sin sacrificar rendimiento. Se cuenta una función de activación no lineal como es 'relu'. Su trabajo consiste en unificar todas las características reconocidas en la imagen y asignar un valor numérico para su posterior clasificación. Para facilitar la comprensión se aporta la siguiente \href{https://slugnet.jarrodkahn.com/layers.html}{ilustración}.
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.6\textwidth]{figures/ia/dense.png}
		\caption{Representación de una capa \textbf{Dense} a nivel conceptual}
		\label{fig:dense}
	\end{figure}
	\item Se añade una capa de \textbf{Dropout} aún más agresiva.
	\item Por último se la función de activación \textit{softmax} ya que es la que mejores resultados obtiene en problemas de clasificación para la capa de salida que constará de tres neuronas, cada una de  ellas refiriéndose a una categoría. Esta capa es de tipo \textbf{Dense}.
	\item Se compila escogiendo como función pérdida el \textit{categorical-crossentropy}, que será la encargada de dictaminar la dependencia entre los resultados ofrecidos por la función \textit{softmax}. Cuando menor sea esta dependencia mejor entrenada está nuestra red y obtendremos un valor de pérdida  más bajo. El parámetro correspondiente a \textit{optimizer} indica el uso de la función \textit{ADAM}, que en resumidas cuentas es una evolución del clásico algoritmo de descenso por gradiente que posibilita el reajuste de pesos en las neuronas durante la retropropagación. En \textit{metrics} añadimos la información que se quiere mostrar durante el entrenamiento, que serán.
	\item Por último queda la fase de entrenamiento, se tienen en cuenta algunos parámetros nuevos devenidos del uso de \textit{ImageDataGenerator} para el procesamiento del dataset. Además de pasarle los datos de validación y entrenamiento debemos especificar cuantos \textit{steps} se realizarán por cada etapa del entrenamiento. Este valor se escoge entorno la división entre el número total de muestras y el tamaño del \textit{batch} previamente escogido. Por ejemplo, al tener 6240 imágenes para el entrenamiento, y haberse dividido en lotes de 30 se obtiene que son necesarios 208 pasos para asegurar que todas las muestras son evaluadas en cada una de las etapas (\textit{epochs}) del entrenamiento. En el siguiente apartado se hablarán de los parámetros restantes que han influido en la mejora del diseño arquitectónico respecto al módulo auxiliar.
\end{enumerate}
	\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{figures/ia/convnetwork.png}
	\caption{Implementación para la red neuronal de Fashionlint}
	\label{fig:convnet}
\end{figure}
Los resultados finales del entrenamiento fueron muy satisfactorios teniendo en cuenta las limitaciones del dataset. Con picos cercanos al 80\% de precisión como se muestra a continuación.
\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{figures/ia/result.png}
\caption{Valores de validación tras el entrenamiento}
\label{fig:result}
\end{figure}


\subsection{Mejoras y ajustes}
Durante todo el proceso de diseño y pruebas, se fueron añadiendo a algoritmo ciertas mejoras incrementales que han repercutido positivamente en la exactitud final del modelo y en otros aspectos como el desempeño. Sólo unas décimas de exactitud entre un modelo y otro, son las que diferencian una buena implementación de una mediocre. Es bien sabido que existen numerosos tutoriales para crear todo tipos de redes neuronales y varios datasets de ejemplo, esto hace posible para el gran público el uso de algoritmos de machine learning de una manera asequible y sencilla. Sin embargo, copiar código de un cuaderno de Jupyter realizado por otro usuario y ejecutarlo en tu máquina no te convierte en un científico de datos, sino más bien en un 'script kiddie'. La ciencia de datos es un universo inmenso cargado de campos diferentes entre sí, esto sumado a un núcleo basado en matemáticas y estadísticas avanzadas.

El proyecto de Fashionlint no ha querido reinventar la rueda, pues la prioridad era sacar un producto comercial, no desarrollar un paper de investigación. Más, si se ha tratado de ahondar en cuestiones teóricas con el fin de saber qué y cómo está ocurriendo en cada momento. En los siguientes puntos se abordarán algunas de las mejoras más interesantes que presenta la implementación:

\subsubsection{Data augmentation}
El \textit{Data augmentation} se conoce como un conjunto de técnicas que, aplicadas a nuestros dataset basado en imágenes consigue aumentar su variabilidad, y por ende la longitud relativa de éste siendo esta última finalidad la que da el nombre a esta práctica. Gracias al \textit{data augmentation} se puede multiplicar hasta en 10 el número de muestras. Incluso cuando el número de muestras es suficiente, la modificación de las fotografías puede ser útil. Entre estas técnicas se encuentran:
\begin{enumerate}
	\item Zoom. Aumento de la imagen.
	\item Recorte. De una imagen se extraen varias de menor superficie que la original.
	\item Rotación. Consiste en desplazar la imagen sobre su su eje central tantos grados como se crea conveniente.
	\item Volteado. Invertir por completo la imagen.
	\item Mirroring. Obtener una nueva imagen que simula el efecto del reflejo de la original respecto a un eje de simetría.
	\item Translación. Mover la figura principal sobre el fondo de la imagen.
	\item Ruido Gaussiano. Añade transformaciones en el rango dinámico de visualización de la fotografía.
	\item Interpolación. Aplicación de las técnicas anteriores rellenando las zonas vacías  expuestas de la imagen en caso de ser necesario/
	\item Filtros. Con ellos se puede variar la luminosidad, saturación, rango de colores, etc... Se pueden encontrar en software de edición profesional pero cuentan con implementaciones en python afortunadamente.
\end{enumerate}
Para nuestro caso hemos hecho uso del zoom y la rotación, por medio de la clase \textit{ImageDataGenerator} del pack de preprocesamiento de Keras como se puede apreciar en la implementación \ref{fig:imagedate} ajustando los parámetros \textit{zoom\_range} y \textit{shear\ range}.
\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{figures/ia/augmentation.png}
\caption{Ejemplo de aumento de datos con diferentes técnicas}
\label{fig:augmentation}
\end{figure}

\subsubsection{Early Stopping}
Uno de los parámetros más controversiales a la hora de encauzar el entrenamiento es el número de etapas, o \textit{epochs}. Esto es debido a que un entrenamiento corto causará baja precisión mientras que uno demasiado largo puede tomar excesivo tiempo sin tener un impacto beneficioso en la precisión del modelo. Como solución al problema, Keras brinda la opción de detectar de manera automática cuando la precisión del modelo ha alcanzado su límite, dando por finalizado el entrenamiento.

Esto ha resultado extremadamente útil en nuestro caso concreto ya que en un principio se hicieron pruebas con 64 etapas que tardan unas tres horas aproximadamente. Lo cual hacia inviable un ajuste a ojo, pues por cada prueba arbitraria el equipo de desarrollo perdía mucho tiempo.

Como se puede apreciar en el ejemplo, se instancia el \textit{EarlyStopping} desde la clase \text{callbacks}. El parámetro \textit{monitor} indica la medida de referencia para parar el entrenamiento, en este caso se busca minimizar (\textit{mode='min'}) el valor proporcionado por la función pérdida y tiene un margen de 4 \textit{patience=4} etapas para reiterarse en su comprobación antes de cerrar el proceso. Con la opción de \textit{restore\_best\_weights} recuperamos fases tempranas en las que quizá el entrenamiento fue más acertado.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{figures/ia/early.png}
	\caption{Instancia de \textit{early stoppping} desde la clase \textit{callbacks}}
	\label{fig:early}
\end{figure}
Por último se asigna esta configuración al parámetro \textit{callbacks} de la función \textit{fit} como se observa en \ref{fig:convnet}.
\subsubsection{Preprocesado de imágenes reales}
Si no es posible conseguir que  las imágenes del dataset se parezcan a las de los usuarios finales, se transformarán las imágenes de los usuarios conforme a las del dataset. Para lograr este propósito se ha recurrido a un modelo desarrollado por terceros llamado \textbf{U2\-Net} \href{https://github.com/NathanUA/U-2-Net}{(link al repositorio)} que consiste en reconocer el objeto principal de la fotografía (modelos o usuario en el caso de Fashionlint).

Una vez se ha eliminado el fondo de la imagen este es sustituido por un tono gris que emula las condiciones del dataset antes de pasar por el modelo de clasificación. Aunque no es buena práctica se accede al script de u2net a través del sistema operativo, esto es debido disparidad entre versiones de ciertos módulos comunes a ambos proyectos. El resultado se puede observar en la fotografía \ref{fig:u2net}.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{figures/ia/u2net.png}
	\caption{Código de transformación de la imagen comentado}
	\label{fig:u2net}
\end{figure}

\subsection{Evaluación de modelos}
Para la evaluación de ambos modelos se ha recurrido al método \textit{evaluate} provisto por \textbf{Keras}. Este método aplicado a un modelo requiere de unos datos de test, que consisten en imágenes categorizadas previamente de manera correcta sobre las que el algoritmo hará predicciones y posteriormente comparará la exactitud de éstas con la categorización real provista en un principio. También es necesario especificar el números de \textit{steps} en caso de haber fraccionado el dataset en \textit{batches}, siguiendo la regla de tres anteriormente mencionada: número de muestras = batch size x steps. Asegurándose la comprobación de todas y cada una de las muestras.

El método \textit{evaluate} devuelve una tupla con dos valores, el primero es valor de la función pérdida y el segundo el porcentaje de acierto en las predicciones.Es importante recalcar que estas muestras deben ser completamente ajenas al entrenamiento para realizar una validación independiente y aislada, que aporte un valor representativo y veraz. En el código siempre se realiza esta comprobación al terminar el entrenamiento del modelo, y justo después de su carga en funcionalidades externas. Puede ser un proceso costoso, por tanto queda reservado para el desarrollo y las pruebas, nunca en la parte de servidor en producción.
\begin{itemize}
	\item Los resultados para el primer modelo presentan una exactitud muy alta y un gran desempeño a la hora de separar modelos de prendas, aunque en algunos casos como en el de los trajes de chaqueta, éste confunde la prenda con silueta humana. Además un pequeñísimo porcentaje de las fotografías no están dentro de ninguna de las categorías, siendo imágenes del tren inferior exclusivamente para pantalones, o del tronco en el caso de prendas como camisas y camisetas. El mejor entrenamiento, con un dataset cercano a las 3000 muestras ofreció una asombrosa precisión en las predicciones del 94.83\% como se puede observar en la figura \ref{fig:modeleval}.
	\item La exactitud del modelo principal ha alcanzado un 77,22\%, teniendo en cuenta la dificultad de distinción entre el estilo casual y el sport, se considera una precisión aceptable pero con mucho margen de mejora. La evaluación se ha realizad con 788 muestras pertenecientes a las 3 categorías. En la siguiente figura podemos observar un ejemplo de evaluación y extracción de información relevante sobre el modelo, entre las que se encuentra: formato y contenido de los lotes para su entrenamiento y validación, precisión, función pérdida y mapeo de categorías para una lectura humana de las predicciones.
\end{itemize}
\begin{figure}[htb]
	\centering
	\includegraphics[width=1\textwidth]{figures/ia/validatefinal.png}
	\caption{Método para la evaluación del modelo entrenado}
	\label{fig:valifinal}
\end{figure}
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\textwidth]{figures/ia/validateoutput.png}
	\caption{Salida por pantalla tras la ejecución del código: \ref{fig:valifinal} }
	\label{fig:valioutput}
\end{figure}