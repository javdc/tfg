%!TEX root =  tfg.tex
\chapter{Conceptos básicos y estructura del proyecto}

\begin{quotation}[Entrepeneur]{Jim Rohn (1930--2009)}
Success is neither magical nor mysterious. Success is the natural consequence of consistently applying the basic fundamentals.
\end{quotation}

\begin{abstract}
	En esta sección se verán algunos conceptos básicos del desarrollo en Android que ayudarán a comprender el proyecto y se hará un recorrido por la estructura y organización de la aplicación desarrollada.
\end{abstract}

\section{Componentes básicos de una aplicación}

Antes de comenzar con la exposición de la estructura del proyecto desarrollado, conviene repasar algunos conceptos básicos y cómo se estructuran en general las aplicaciones Android. Una forma sencilla de comprenderlo es creando un nuevo proyecto vacío en Android Studio, con una sola activity y sin funcionalidad más allá de mostrar una pantalla en blanco.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/android-introduccion-estructura-codigo/empty-project.png}
	\caption{Archivos de un proyecto vacío}
	\label{fig:emptyProject}
\end{figure}

Como se puede observar en la figura \ref{fig:emptyProject}, una aplicación contiene los siguientes tipos de recursos: el código funcional, escrito en Java o Kotlin, que se encuentra en la carpeta \textit{java}; los recursos, que se encuentran en la carpeta \textit{res}; el manifest, que describe la aplicación y sus componentes y por último los scripts de Gradle, que nos permite añadir librerías al proyecto y configurar la compilación. Veamos cada uno de ellos con más detenimiento

\subsection{Código fuente}

Para que la aplicación pueda funcionar correctamente, como es lógico, necesitamos un código fuente con toda la lógica necesaria. Aquí es donde se escribe todo el código necesario, tanto para las vistas como . Por lo general, las distintas clases se mantienen en distintos subpaquetes para mantener un orden.

Podemos ver cómo ya hay creado con antelación un MainActivity. Una activity representa una pantalla individual de una interfaz de usuario. Por ejemplo, una aplicación de chat podría tener una activity para la lista de conversaciones, otra para una conversación individual y otra para los ajustes. Cada activity, junto a su correspondiente clase en Java o Kotlin, tiene asignado un layout que define los elementos con los que cuenta esta pantalla.

Además de las activities tenemos los fragments, que no son más que una vista que se puede embeber dentro de una activity y cuenta con algún layout asociado. Los fragments aparecieron con la idea de poder definir interfaces de tablet, cuyas interfaces suelen tener varios elementos distintos en pantalla al ser esta más grande, de una forma más sencilla. Sin embargo, hoy en día tiene más usos, como por ejemplo definir un menú lateral con varias secciones: el menú lateral se encontraría en la activity, y las secciones que abre serían distintos fragments en esta activity.

Las clases de las vistas se ejecutan en un hilo específico que se encarga de renderizar los datos en pantalla, por lo que estas idealmente deben contener únicamente el código correspondiente a la vista en sí, mientras que la extracción y procesado de datos debería realizarse en un hilo distinto. Para ello, las corrutinas de Kotlin, que veremos más adelante, resultan de gran utilidad.

\subsection{Recursos}

Dentro de la carpeta \textit{res} podemos encontrar todo tipo de recursos de los que hace uso la aplicación. En \textit{layouts} se definen las distintas vistas de las que se compone una activity o un fragment, generalmente en un ConstraintLayout, que ayuda a ajustar la disposición de los elementos entre ellos y según el tamaño de la pantalla, de forma que las vistas se ajusten a todo tipo de dispositivos. Además, dentro de \textit{values} podemos definir los textos de la aplicación y sus traducciones en strings.xml, o el tema en styles.xml, entre otros. Por último, en \textit{mipmap} y \textit{drawable} es donde podemos guardar las imágenes rasterizadas o vectoriales, respectivamente. Por lo general, los resources se definen en archivos XML.

\subsection{Manifest}

El manifest (definido en el archivo AndroidManifest.xml) contiene la información necesaria para que Android sepa cómo ejecutar la aplicación. Aquí es donde se definen el nombre de la aplicación, los permisos que utiliza (como la cámara o el almacenamiento) y las activities, servicios, receptores y proveedores de los que se compone la aplicación, entre otra información adicional.

\subsection{Scripts de Gradle}

Gradle es un sistema automático de compilación del cual hace uso Android Studio para compilar la aplicación. Contamos con dos archivos \textit{build.gradle}, uno a nivel de proyecto y otro a nivel de módulo. En el primero se definen algunas variables globales y que se usan en todos los módulos que use la aplicación (en el caso de Fashionlint solo cuenta con uno), como la versión de Kotlin o los repositorios para las librerías. En el de módulo, es donde definimos algunas opciones de compilación como la versión de SDK a utilizar y las dependencias que tendrá nuestro proyecto, es decir, las librerías que urilizaremos, tanto las pertenecientes a Android como las de terceros.

\section{Estructura y funcionamiento de la aplicación desarrollada}

Una vez aclarados los componentes básicos de toda aplicación Android, se puede proceder a exponer cómo se han organizado los distintos componentes de la aplicación desarrollada. La aplicación de Fashionlint se ha diseñado desde el principio con la idea de que no solo cumpla con la funcionalidad necesaria, sino que además cuente con una base robusta, ordenada e implementada de la mejor manera posible para facilitar la extensión de funcionalidad y permitir un buen mantenimiento en el futuro.

El código de la aplicación se ha estructurado en distintos subpaquetes según la funcionalidad de las clases que contenga. Así, es bastante sencillo encontrar la clase adecuada según lo que necesitemos modificar. La estructuración final del proyecto se puede examinar en la figura \ref{fig:fashionlintPackages}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\textwidth]{figures/android-introduccion-estructura-codigo/paquetes-fashionlint.png}
	\caption{Subpaquetes dentro de la aplicación}
	\label{fig:fashionlintPackages}
\end{figure}

Como la aplicación sigue el patrón de arquitectura Model-view-viewmodel contamos, aparte de algunos términos ya conocidos como activity o fragment, con otros términos no expuestos hasta ahora como repository o viewmodel. Este patrón arquitectónico, que es lo que nos permite la robustez expuesta arriba, se detallará a fondo en el siguiente capítulo.

En la figura \ref{fig:acivitiesFragments} se pueden ver las activities y fragments de los que se compone la aplicación.

\begin{figure}[ht]
	\subfigure[Activities]{
		\centering
		\includegraphics[width=0.45\textwidth]{figures/android-introduccion-estructura-codigo/activities.png}
	}
	\hfill
	\subfigure[Fragments]{
		\centering
		\includegraphics[width=0.45\textwidth]{figures/android-introduccion-estructura-codigo/fragments.png}
	}
	\caption{Activities y fragments con los que cuenta la aplicación}
	\label{fig:acivitiesFragments}
\end{figure}

La pantalla principal de la app es MainActivity. Esta, mediante un \textit{navigation drawer} o menú lateral, figura \ref{fig:navigationDrawer}, está compuesta por los tres fragments que se pueden ver en la figura de la derecha: el de la lista completa de outfits, el de la lista de outfits favoritos, y el de la lista de outfits creados. Esta MainActivity, cada vez que se abre la aplicación, comprueba si el usuario está logueado (comprobando si hay un token guardado en las shared preferences, usado en Android para guardar datos básicos de la aplicación), y si no es el caso reenvía al usuario a la vista de inicio de sesión, que se encuentra en LoginActivity, figura \ref{fig:loginActivity}.

\begin{figure}[ht]
	\hfill
	\subfigure[Navigation drawer]{
		\centering
		\includegraphics[width=0.4\textwidth]{figures/android-introduccion-estructura-codigo/drawer.png}
		\label{fig:navigationDrawer}
	}
	\hfill
	\subfigure[Inicio de sesión]{
		\centering
		\includegraphics[width=0.4\textwidth]{figures/android-introduccion-estructura-codigo/login.png}
		\label{fig:loginActivity}
	}
	\hfill
	\caption{Menú lateral de la aplicación y pantalla de inicio de sesión}
\end{figure}

Además, haciendo uso de la librería \href{https://github.com/AppIntro/AppIntro}{AppIntro} tenemos una pantalla de bienvenida (en WelcomeActivity) explicando el uso básico de la aplicación, haciendo de pequeño asistente para el usuario novel, figura \ref{fig:welcomeActivity}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\textwidth]{figures/android-introduccion-estructura-codigo/welcome.png}
	\caption{Pantalla de inicio para usuarios nuevos}
	\label{fig:welcomeActivity}
\end{figure}

Las listas mostradas en pantalla se renderizan mediante un \href{https://developer.android.com/guide/topics/ui/layout/recyclerview}{RecyclerView}, una librería para mostrar listas en pantalla de manera optimizada, manteniendo en memoria solo los elementos que están a la vista. Para la implementación de un RecyclerView se necesita, primero, añadir el elemento RecyclerView en el layout correspondiente, por otra parte crear un layout nuevo que defina los componentes de cada elemento de la lista (que, en nuestro caso, son los que comienzan por \textit{row\_}) y, por último, crear una clase Adapter e inicializar el RecyclerView con ese Adapter en la Activity o Fragment correspondiente. Cada outfit se puede pulsar, lo que nos redigirá a la vista de detalles, \ref{fig:detailActivity}, donde podremos ver más información del outfit correspondiente, como la descripción o el precio de la prenda si corresponde.

\begin{figure}[ht]
	\hfill
	\subfigure[Listado de outfits]{
		\centering
		\includegraphics[width=0.4\textwidth]{figures/android-introduccion-estructura-codigo/outfits.png}
		\label{fig:outfitsList}
	}
	\hfill
	\subfigure[Vista de detalle]{
		\centering
		\includegraphics[width=0.4\textwidth]{figures/android-introduccion-estructura-codigo/details.png}
		\label{fig:detailActivity}
	}
	\hfill
	\caption{Listado de outfits y vista de detalle al pulsar en uno}
\end{figure}

A su vez, para la visualización de imágenes en pantalla en todas las pantallas de la aplicación se hace uso de la librería \href{https://github.com/bumptech/glide}{Glide}. Esta es una conocida y veterana librería para cargar imágenes de manera optimizada en Android. Gracias a Glide, la aplicación puede contar con una caché de imágenes y no tener problemas de memoria o congelaciones a la hora de abrir archivos grandes.

En la lista de outfits principal tenemos un Floating Action Button (botón posicionado abajo a la derecha que representa la acción principal de la pantalla) mediante el cual podemos acceder a la cámara, que se encuentra en CameraActivity, figura \ref{fig:cameraActivity}. Para la implementación de esta funcionalidad se ha hecho uso de la librería \href{https://developer.android.com/training/camerax}{CameraX}. Esta novedosa librería perteneciente a Android Jetpack es de gran ayuda a la hora de implementar funciones de cámara en la aplicación. Internamente usa las funciones de Camera2, una API interna de Android para acceder al harware de la cámara, pero mientras que el acceso a Camera2 resulta tedioso y, en ocasiones, problemático debido a las diferencias de implementación en distintos fabricantes, CameraX usa un enfoque más simple, resuelve automáticamente problemas de compatibilidad con dispositivos y, como todas las librerías de Jetpack, tiene en cuenta el ciclo de vida, que como veremos en el siguiente capítulo resulta de gran importancia.

\begin{figure}[ht]
	\hfill
	\subfigure[Cámara]{
		\centering
		\includegraphics[width=0.4\textwidth]{figures/android-introduccion-estructura-codigo/camera.png}
		\label{fig:cameraActivity}
	}
	\hfill
	\subfigure[Selector de imágenes]{
		\centering
		\includegraphics[width=0.4\textwidth]{figures/android-introduccion-estructura-codigo/filepicker.png}
		\label{fig:filePicker}
	}
	\hfill
	\caption{Vista de cámara y selector de imágenes}
\end{figure}

Además, en la vista de la cámara se ha incluido un botón para activar la cámara frontal y otro para poder cargar una imagen desde el almacenamiento mediante el selector de archivos interno de Android, figura \ref{fig:filePicker}, con total compatibilidad con el llamado \textit{scoped storage} para ser compatible con el futuro Android 11, que tendrá limitaciones en el acceso al almacenamiento interno para las aplicaciones. También esta activity contiene la lógica para pedir los permisos necesarios al usuario para acceder a la cámara con la ayuda de \textit{ContextCompat}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.4\textwidth]{figures/android-introduccion-estructura-codigo/send-outfit.png}
	\caption{Pantalla para enviar outfit}
	\label{fig:sendOutfitActivity}
\end{figure}

Una vez se ha realizado la imagen o se ha seleccionado el archivo, se muestra al usuario la pantalla de envío del outfit al servidor, que está manejada por CreateOutfitActivity, figura \ref{fig:sendOutfitActivity}. Aquí se muestra la foto con el fondo recortado por el servidor, y el usuario puede añadir un nombre y una descripción y enviarlo si está conforme, o volver a la vista de cámara para hacer otra foto. Una vez enviada, el usuario es redirigido a la vista de recomendación de outfits, donde se muestran outfits relacionados con el estilo que el módulo de inteligencia artificial en el servidor ha podido detectar para que el usuario pueda tomar inspiración, en figura \ref{fig:recommendationActivity}. Estos outfits pueden ser tanto de fuentes como Zalando como de otros usuarios.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.4\textwidth]{figures/android-introduccion-estructura-codigo/recommend.png}
	\caption{Pantalla de cálculos de estilo y recomendaciones}
	\label{fig:recommendationActivity}
\end{figure}

Por último, ya que el servidor se ejecuta de manera local actualmente y la dirección IP de este podría ser distinta según el ordenador donde se ejecute, se ha añadido una vista de ajustes (figura \ref{fig:hostSettings}) de la aplicación, con la ayuda de \textit{PreferenceFragmentCompat}, en la que se puede cambiar el host y el puerto del servidor al que se debe conectar.
	
\begin{figure}[htb]
	\hfill
	\subfigure[Pantalla de ajustes]{
		\centering
		\includegraphics[width=0.4\textwidth]{figures/android-introduccion-estructura-codigo/settings.png}
	}
	\hfill
	\subfigure[Diálogo para cambiar la dirección a conectar]{
		\centering
		\includegraphics[width=0.4\textwidth]{figures/android-introduccion-estructura-codigo/ip.png}
	}
	\hfill
	\caption{Ajustes en la aplicación para modificar la dirección donde se encuentra el servidor}
	\label{fig:hostSettings}
\end{figure}

% Todas estas activities y fragments cuentan con sus correspondientes layouts, definidos en sus respectivos archivos XML. En general se ha usado ConstraintLayout para las pantallas con una interfaz más compleja, y para las actividades más simples unos LinearLayout o FrameLayout.