%!TEX root =  tfg.tex
\chapter{Patrones de arquitectura e implementación en el proyecto}

\begin{quotation}[Software Engineer]{Eoin Woods}
Software architecture is the set of design decisions which, if made incorrectly, may cause your project to be cancelled.
\end{quotation}

\begin{abstract}
	En este capítulo se detalla en qué consiste un patrón de arquitectura, se repasan los patrones más utilizados, así como los Architecture Components, en el contexto del desarrollo en Android y cómo se adapta nuestro proyecto al patrón Model-view-viewmodel.
\end{abstract}

\section{Introducción a los patrones de arquitectura}

Un patrón de arquitectura (también llamado patrón arquitectónico) es una solución reutilizable a un problema recurrente en la arquitectura de software. A diferencia de los patrones de diseño, los patrones de arquitectura son mucho más generales: mientras que los patrones de diseño buscan resolver un subproblema específico y generalmente pequeño, los patrones arquitectónicos buscan influir en la organización e interacción entre los distintos componentes de una aplicación.

Cabe recalcar que los patrones de arquitectura no son manuales estrictos acerca de cómo organizar todos los apartados de una aplicación. Por eso, dos proyectos de software desarrollados teniendo en cuenta el mismo patrón arquitectónico podrían, a simple vista, parecer proyectos totalmente distintos. Con un patrón de arquitectura no solo se busca ajustar el proyecto a ese patrón, sino además ajustar el patrón a las necesidades del proyecto.

\section{Patrones de arquitectura en Android}

Aunque el término patrón de arquitectura es muy general y puede referirse a cualquier tipo de solución aplicada a la ingeniería de software, en Android se suelen utilizar dos tipos de patrones distintos para el diseño y el desarrollo de aplicaciones: Model View Presenter (MVP) y Model-view-viewmodel (MVVM). Este último, aunque tiene sus orígenes en Microsoft como veremos más adelante, ha tomado especial importancia estos últimos años en el diseño de aplicaciones Android ya que Google ha creado dentro del conjunto de librerías Android Jetpack los Android Architecture Components, un conjunto de componentes destinado a ayudar a implementar este patrón de forma más sencilla teniendo en cuenta el ciclo de vida de las aplicaciones y su persistencia de datos\cite{architecturecomponents}.

Aunque MVVM es ya el patrón recomendado para el desarrollo de aplicaciones, MVP sigue siendo utilizado en un importante número de aplicaciones, sobre todo aquellas cuyo desarrollo comenzó antes de la aparición de los Architecture Components. A continuación veremos en qué consisten ambos y cómo el patrón MVVM puede ayudar a solucionar varios problemas recurrentes en el desarrollo de aplicaciones Android.

\section{Model View Presenter}

El patrón Model View Presenter es un derivado del ya conocido patrón MVC (Model View Controller). En el patrón MVP, el presentador actúa como un intermediario, toda la lógica de presentación está guiada por el presentador. En comparación con el patrón MVC, el modelo está más separado de la vista, porque el presentador actúa como un mediador. El patrón MVP no se trata de un manual a seguir, sino que cada aplicación puede implementarlo según se ajuste a sus necesidades y, por tanto, puede diferir entre distintos proyectos. Sin embargo, las siguientes características básicas son las mismas en todas ellas:

\begin{itemize}
	\item Presentador: El presentador actúa como intermediario entre la vista y el modelo. Recibe los datos del modelo y la lógica de negocio y los reenvía formateados a la vista para su visualización. El presentador tiene la referencia de la vista y el modelo.
	\item Vista: La vista es la visualización de los datos y dirige las entradas y eventos del usuario al presentador. Así que la vista tiene una referencia al presentador.
	\item Modelo: El modelo es, en primer lugar, la representación de los datos. Sin embargo, esto también incluye la lógica de negocio, que en una buena arquitectura debe subdividirse y estructurarse en diferentes componentes: por ejemplo, en una especie de arquitectura de capas o mediante la aplicación de casos de uso a través de interactores.
\end{itemize}

Esta separación de tareas permite probar mediante pruebas unitarias el correcto funcionamiento de la lógica de negocio sin tener que ejecutar toda la lógica referente a la vista. De esta manera, cualquier problema se puede analizar y corregir de forma más fácil. Si no, sería necesario hacer tests de instrumentación para poder probar la aplicación, algo mucho más costoso y dependiente de código que no está relacionado con la lógica de negocio.

Otra ventaja es que el presentador especifica el comportamiento de la interfaz de usuario pero no depende de la interfaz de por sí. Por lo tanto, la UI es fácilmente reemplazable por componentes más nuevos o inluso por otro tipo de vistas para plataformas o dispositivos distintos.

En resumen, el patrón Model View Presenter busca romper el acoplamiento con la capa de presentación. Esto permite que esta capa sea testeable y esté claramente estructurada. Así podemos aplicar principios como la Separación de Preocupaciones de forma más sencilla, facilitando el desarrollo de una arquitectura Clean en nuestro proyecto.

\section{Model-view-viewmodel}

El patrón Model-view-viewmodel es también un derivado del patrón MVC y, como veremos, se asemeja bastante a MVP. Este patrón de arquitectura fue inicialmente diseñado para las plataformas Windows Presentation Foundation y Microsoft Silverlight, pero a lo largo del tiempo se ha ido adaptando, a su manera, a otras plataformas.

En este patrón, la vista está activa con comportamientos, eventos y unión de datos (data binding en inglés), y la vista se sincroniza con el ViewModel, que permite la separación de la presentación y expone métodos y comandos para manejar y manipular el modelo.

MVVM cuenta con tres componentes centrales:
\begin{itemize}
	\item Modelo, que representa los datos con validación y lógica de negocio.
	\item Vista, que es responsable de definir la estructura, el diseño y la apariencia de lo que el usuario ve en pantalla.
	\item ViewModel, que separa la Vista del Modelo, y expone los métodos y comandos para manipular los datos del modelo.
\end{itemize}

La Vista recibe datos del ViewModel a través de métodos y databinding, y durante la ejecución del programa, la vista cambiará cuando responda a eventos en el ViewModel mediante observables. El ViewModel media entre la vista y el modelo y maneja la lógica de la vista. Interactúa con el modelo tomando los datos de este y presentándolos a la vista para su visualización.

Todos estos componentes están desacoplados entre sí, lo que permite una mayor flexibilidad para trabajar en ellos de forma independiente, aislar las pruebas unitarias e intercambiarlas, sin afectar a ningún otro componente, al igual que el patrón MVP.

Esta estructura permite que el modelo y otros componentes evolucionen independientemente, permitiendo a los desarrolladores trabajar en diferentes aspectos de la solución de forma simultánea.  Por ejemplo, cuando los diseñadores trabajan en la Vista, simplemente generan muestras de datos sin necesidad de acceder a los otros componentes.

Ambos patrones arquitectónicos son muy similares y a simple vista difíciles de discernir. La principal diferencia entre ambos es que en el caso de MVP el presentador se comunica con su vista correspondiente mediante una interfaz, mientras que en el caso de MVVM ofrece unos datos mediante eventos que pueden ser observados por varias vistas. Es decir, la relación en MVP es uno a uno, mientras que en MVVM puede ser uno a muchos, como se puede observar en la figura \ref{fig:patternRelationships}. En el caso de Android MVVM resulta mucho más útil, ya que, como se verá más adelante, gracias a los Architecture Components se puede implementar de forma más sencilla teniendo en cuenta el ciclo de vida de la aplicación.

\begin{figure}[ht]
	\subfigure[Patrón MVP]{
		\centering
		\includegraphics[width=0.45\textwidth]{figures/android-patrones-arquitectura/mvp-relation.png}
	}
	\hfill
	\subfigure[Patrón MVVM]{
		\centering
		\includegraphics[width=0.45\textwidth]{figures/android-patrones-arquitectura/mvvm-relation.png}
	}
	\caption{Relación entre los componentes de los patrones de arquitectura\cite{mvpmvsvvm}}
	\label{fig:patternRelationships}
\end{figure}

\section{Ciclos de vida y sus problemas}

Como hemos visto, patrones arquitectónicos como MVP o MVVM resultan de gran utilidad para conseguir que el software que queramos desarrollar esté separado y compartimentado para facilitar la testeabilidad o la reutilización de código en distintas plataformas. Sin embargo, en Android es especialmente útil porque puede evitar tener que lidiar con ciertos problemas que solo aparecen en este tipo de plataformas.

Una app de escritorio o una web, por ejemplo, no cuentan con las mismas limitaciones que un dispositivo móvil. En este último, es necesario tener en cuenta ciertos aspectos como el ahorro de batería, cambios en la conexión de datos o un tipo de multitarea distinta, en la que por ejemplo una llamada puede hacer que nuestra aplicación pase a estar en segundo plano sin consumir recursos, para dentro de un rato volver a ser utilizada donde se dejó. En Android, las aplicaciones, así como sus actividades y fragmentos tienen un ciclo de vida determinado. Estos pasan por un conjunto de estados, y si estos no se tienen en cuenta podemos provocar el tan temido mensaje de ``la aplicación se ha detenido''.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\textwidth]{figures/android-patrones-arquitectura/activity-lifecycle.png}
	\caption{Diagrama del ciclo de vida de una actividad\cite{acticitylifecycle}}
	\label{fig:activityLifecycle}
\end{figure}

Como se puede ver en la figura \ref{fig:activityLifecycle}, desde que se abre hasta que se cierra una actividad esta puede pasar por muchos estados. Esto se magnifica todavía más con los fragments, que tienen un número de estados mayor en su ciclo de vida, que se tienen que sumar al de la actividad en la que se encuentren.

Pongamos un ejemplo en el que no tener en cuenta el ciclo de vida podría provocar un mal funcionamiento de la aplicación. Imaginemos que un usuario entra en la aplicación de Fashionlint para ver los últimos outfits que se han añadido. Sin embargo, no cuenta con una buena conexión a internet y la carga de los outfits es más lenta de lo esperada. Mientras espera, el usuario recibe un mensaje de un amigo, y para responderle pulsa en la notificación para abrir la otra aplicación, dejando la nuestra en segundo plano. La activity con los outfits pasaría a estar en estado pausado, pero la aplicación seguiría en estado de ejecución. Finalmente, la carga de los outfits termina, y el método que se encargó de contactar con la API procede a mostrar la información en pantalla. Como la aplicación está en segundo plano, y por lo tanto la vista está en estado pausado, la aplicación crashea y se muestra el mensaje de que la aplicación se ha detenido.

De hecho, algo tan sencillo como rotar la pantalla también provoca cambios en el ciclo de vida de una aplicación, y si por ejemplo el usuario hubiese escrito un largo texto y la pantalla se rotase, el contenido de este cuadro de texto también se perdería.

Una de las formas de paliar este problema es tener en cuenta todos estos casos y añadir la funcionalidad necesaria mediante \textit{overrides} a los métodos correspondientes a los distintos estados de vida, como \textit{onPause()} u \textit{onDestroy()}, pero esto puede resultar muy tedioso y no tener en cuenta todos los casos posibles por un simple error humano. En este contexto es donde aparecen los Architecture Components.

\section{MVVM en Android: Architecture Components}

Los Android Architecture Components son un conjunto de librerías, pertenecientes a Android Jetpack, que ayudan a diseñar aplicaciones con una base sólida que puedan someterse a prueba y admitan mantenimiento. Cuenta con clases para administrar el ciclo de vida y manejar la persistencia de datos.\cite{architecturecomponents}

Los principales componentes de Architecture Components son los siguientes:
\begin{itemize}
	\item Lifecycle: es una clase que ayuda a controlar mediante observers y otros métodos los distintos estados del ciclo de vida de la aplicación.
	\item LiveData: es un contenedor de datos observable optimizado para tener en cuenta los ciclos de vida.
	\item ViewModel: se encarga de almacenar datos relacionados con la interfaz teniendo en cuenta el ciclo de vida.
	\item Room: librería para ayudar con la persistencia de datos en una base SQLite que devuelve objetos observables por LiveData.
\end{itemize}

Además, en la documentación se ofrece una guía para implementar la arquitectura recomendada haciendo uso de estos componentes en una aplicación Android. Estas pautas se basan en dos principios: la separación de problemas, que en este contexto significa sobre todo mantener el código de las activities y los fragments lo más limpio y separado del resto de funcionalidad que sea posible; y controlar la UI desde un modelo de persistencia, para que los datos mostrados en pantalla no se vean afectados por el ciclo de vida.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\textwidth]{figures/android-patrones-arquitectura/mvvm-architecture-components.png}
	\caption{Diagrama de interacción de los distintos módulos de una aplicación con los Architecture Components}
	\label{fig:archComponents}
\end{figure}

La figura \ref{fig:archComponents} muestra, de forma esquemática, la separación de módulos e interacciones a aspirar en el desarrollo de una aplicación con los Architecture Components. Se puede observar cómo cada componente solo depende del componente que está justo debajo, siendo solo el repositorio el que depende de dos clases, el modelo y la fuente de datos remota.

Este diseño permite que, independientemente de que el usuario vuelva a abrir la aplicación varios minutos o varios días más tarde o de si tiene o no conexión de red, verá la información guardada a nivel local, para poder actualizar los datos en segundo plano si se considera que los datos mostrados en pantalla están desactualizados.

A continuación veremos brevemente los pasos para implementar esta arquitectura.

\subsection{Interfaz de usuario}

La interfaz de usuario se define en las activities o fragments y sus layouts correspondientes. Estos deberían tener, siempre que sea posible, solo la lógica relacionada con el pintado de datos en pantalla, y no realizar ningún cálculo relacionado con los datos en sí. Estos datos deben obtenerse desde el ViewModel, que se leerán desde un observador.

Este diseño se basa en el llamado patrón observador. Este patrón consiste, a grandes rasgos, en que un objeto mantiene una lista de sus dependencias, llamadas observadores, y las notifica automáticamente de cualquier cambio en estos datos. La diferencia principal con el procedimiento clásico de llamar a un método de otra clase es que no tenemos que tener en cuenta cuál es el estado de esa clase a la que vamos a llamar al método ni es necesario mantener una instancia de ella, evitando fugas de memoria innecesarias.

Estos observadores reciben eventos que provienen del ViewModel con la ayuda de LiveData, que se verá con más detenimiento a continuación

\subsection{ViewModel y LiveData}

El ViewModel se encarga de proporcionar datos al componente de la interfaz, que puede ser un fragment o una activity, y también puede incluir lógica de negocio para comunicarse con el modelo. El ViewModel se inicializa desde la vista, pero este no tiene conocimiento de la vista en sí, de manera que no se ve afectado por lo que ocurra a esa vista, como por ejemplo la recreación de la activity debido a la rotación del dispositivo. Su ciclo de vida no depende del ciclo de vida de la vista, como podemos ver en la figura \ref{fig:viewmodelLifecycle}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/android-patrones-arquitectura/viewmodel-lifecycle.png}
	\caption{Diagrama del ciclo de vida de un ViewModel respecto al de una Activity}
	\label{fig:viewmodelLifecycle}
\end{figure}

El ViewModel necesita de alguna forma ofrecer a la vista un objeto que se pueda observar. Aquí es donde entra en juego LiveData. LiveData permite poder observar los cambios de un recurso, de nuevo, teniendo en cuenta el estado del ciclo de vida de los componentes de la aplicación, además de contar con lógica para la limpieza de memoria y evitar fugas de memoria de forma automática.

El funcionamiento de estos componentes es el siguiente. El ViewModel específico de la activity o fragment estaría compuesto de varios métodos para obtener distinta información. Estos métodos devolverían un LiveData instantáneamente, que pasa a ser observado por la activity o fragment. Este LiveData, que proviene de hacer otra llamada al repositorio, va cambiando según se vayan cargando los datos de la fuente apropiada, que puede ser o bien la base de datos local o los datos obtenidos de una API. Cuando estos datos cambian en el LiveData, la UI recibe este evento mediante el observer y procede a mostrar los datos nuevos al usuario.

\subsection{Repositorio}

El repositorio consta de todos los métodos que manejan las operaciones de datos. A efectos prácticos, actúa como una API a la que el ViewModel puede pedir datos y poder recibirlos, independientemente de que estos datos provengan de una base de datos local o de un servidor externo. Esto facilita muchísimo mantener el código limpio, legible y ordenado. De cara al ViewModel, el repositorio solo devuelve un LiveData que contendrá los datos que se han pedido, y este hace de mediador encargándose de llamar a la fuente de datos correcta o manejar una caché, por ejemplo.

En condiciones óptimas, el repositorio devuelve instantáneamente un LiveData independientemente de si los datos han cargado o no, para posteriormente ir emitiendo cambios a este LiveData con los datos que se carguen de la fuente externa. Con esto conseguimos que la aplicación no se congele, ya que estos métodos podrían ser bloqueantes y se ejecutan en el mismo hilo encargado del renderizado de la UI. Para evitar este problema, una solución es hacer uso de las corrutinas de Kotlin, que veremos más adelante.

\subsection{Fuente de datos remota con Retrofit}

El repositorio, junto con los datos locales, se nutre de una fuente externa, es decir, una API de un servidor que nos facilite los datos necesarios. Para ello, la guía de Architecture Components recomienda el uso de la librería Retrofit.

Retrofit es una librería que hace de cliente HTTP y facilita, mediante anotaciones en el código, tanto la conexión en sí al servidor como el parseado de la respuesta en clases DTO creadas en el código.\cite{retrofitdocs} Esta librería, mantenida por la compañía Square, disfruta de una gran madurez (cuenta con más de 7 años de desarrollo) y es extremadamente popular, por lo que no resulta difícil encontrar documentación y ayuda abundantes sobre esta librería.

Además de callbacks, Retrofit también soporta el uso de corrutinas, lo cual permite que podamos utilizar esta tecnología en todos los métodos del repositorio, tanto los de fuente externa como los de la base de datos local, teniendo un código mucho más consistente.

\subsection{Modelo con Room}

Como se ha comentado anteriormente, debemos tener en cuenta todos los comportamientos relativos al ciclo de vida de una aplicación. Si un usuario, después de cargar los datos de una API, cerrase la aplicación para abrirla horas más tarde, debería ser capaz de ver esa información de la que disponía antes, independientemente de su calidad de conexión y sin esperas. Aquí es donde entra en juego Room.

Room es una librería que ofrece persistencia de datos local de una forma más sencilla que acceder a la base de datos de forma directa. Como todas las librerías de Architecture Components, tiene en cuenta los ciclos de vida y, por supuesto, es capaz de devolver LiveDatas que notifiquen de cambios en la base de datos automáticamente, evitando incongruencias al leer de la base de datos en distintas ocasiones y nos permite poder seguir la práctica ``Single Source of Truth''.

Para poder usar Room hace falta definir, por una parte, una entidad, mediante una data class y la anotación \textit{@Entity}, la base de datos con la anotación \textit{@Database} y por último las queries que necesitemos en el DAO (Data Access Object), con la anotación del mismo nombre. Esto se verá con más detalle en la implementación concreta de nuestro proyecto.

\section{Implementación en el proyecto}

Una vez detallados todos los términos y vista la carga teórica del patrón MVVM y los Architecture Components, se procede a detallar cómo se ha implementado este patrón de arquitectura en la aplicación de Fashionlint.

\subsection{Base de datos con Room}

Para la base de datos, que recordamos está implementada con la librería Room, contamos con las clases mostradas en la figura \ref{fig:databaseClasses}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.35\textwidth]{figures/android-patrones-arquitectura/database.png}
	\caption{Clases relativas a la base de datos con Room}
	\label{fig:databaseClasses}
\end{figure}

En primer lugar, tenemos la instanciación de la base de datos en \textit{OutfitDatabase}. Mediante el método getInstance, que hace uso del método \textit{syncronized} de Kotlin, podemos obtener la instanciación única de la base de datos. Una vez creada por primera vez, las siguientes llamadas a getInstance devolverán la base de datos ya creada, evitando \textit{memory leaks} y otros problemas. En el código se puede comprobar cómo en el constructor el método \textit{allowMainThreadQueries()} está comentado. Este método, utilizado solo al principio como pruebas, permite la práctica no recomendada de acceder a la base de datos mediante el hilo de renderizado de la interfaz, es decir, si se ejecutase directamente en el código. Como se comentó en el anterior capítulo, toda funcionalidad que pueda ser bloqueante no debe ejecutarse en este hilo, sino en un hilo aparte. Para acabar con esta problemática de la mejor manera posible, se han usado las corrutinas de Kotlin, que se verán con más detalle en la sección del repositorio. El hecho de no usar este método del constructor asegura que se está usando la base de datos de forma correcta.

Continuamos con la clase \textit{OutfitDomain}. En esta clase es donde se ha definido la entidad outfit. No es más que una \textit{data class} con todos los atributos que cuenta cada outfit. Al utilizar Kotlin, si un atributo es opcional basta con añadir un ``?'' al final del tipo. En nuestro caso, tan solo la id, que es autogenerada, y el nombre son obligatorios.

En la clase \textit{Converters} es donde se encuentran los conversores necesarios para poder guardar tipos que no son compatibles con SQLite. En nuestro caso, solo tenemos un conversor de lista de strings a strings y viceversa, utilizado para poder guardar una lista de imágenes en la base de datos para un mismo outfit.

Por último, la interfaz \textit{OutfitDatabaseDao} es probablemente la más interesante. Aquí es donde se definen las distintas operaciones que podemos realizar a la base de datos mediante \textit{queries}. Veamos un ejemplo:

\lstset{language=Kotlin}
\begin{lstlisting}
@Query("SELECT * FROM outfits ORDER BY name ASC")
suspend fun getAllOrderByName(): List<OutfitDomain>
\end{lstlisting}

En primer lugar, mediante la anotación \textit{@Query} se define la query concreta que queremos utilizar, y justo debajo definimos el método que ejecuta esa query con la sintaxis normal de Kotlin. Además, en todos los métodos se hace uso de la palabra clave \textit{suspend}: esto permite que los métodos se puedan ejecutar dentro de una corrutina.

De la misma manera, tenemos la anotación \textit{@Insert} para definir los métodos que insertan outfits nuevos en la base de datos, pudiendo definir la estrategia de en caso de conflicto.

\begin{lstlisting}
@Insert(onConflict = OnConflictStrategy.REPLACE)
suspend fun insert(outfit: OutfitDomain)
\end{lstlisting}

\subsection{Conexión con la API por Retrofit}

Dentro del subpaquete \textit{api} tenemos todas las clases e interfaces necesarias para poder contactar con la API del servidor mediante Retrofit y parsear las respuestas de manera apropiada. Figura \ref{fig:apiClasses}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.35\textwidth]{figures/android-patrones-arquitectura/api.png}
	\caption{Clases e interfaces usadas para la API externa}
	\label{fig:apiClasses}
\end{figure}

Como con la base de datos, contamos con RetrofitInstance, que nos devuelve la instancia de Retrofit para usar en la aplicación inicializada con una URL base, que en nuestro caso se introduce mediante un método para poder cambiarla desde los ajustes de la aplicación. Gracias a la palabra clave \textit{object}, Kotlin mantiene una sola instancia del objeto retrofit.

Por otra parte, tenemos \textit{FashionlintWs}, una interfaz que contiene todos los métodos que nos permiten contactar con la API de Fashionlint, de manera similar a como hace \textit{OutfitDatabaseDao} con la base de datos. Mediante las anotaciones de Retrofit @GET, @POST y demás podemos definir el tipo de petición que queremos hacer y a qué endpoint nos queremos conectar. Por ejemplo:

\begin{lstlisting}
@POST("/api/auth/login")
fun login(@Body login: LoginDto): Call<LoginResponseDto>
\end{lstlisting}

Como se puede observar, estos métodos tienen tanto de entrada como salida unos objetos DTO. Estos \textit{data transfer objects} nos permiten parsear la respuesta JSON devuelta por el servidor en un objeto accesible directamente en el código. Son, de nuevo, \textit{data classes} que contienen los atributos que necesitamos obtener del JSON devuelto con las anotaciones \textit{@SerializedName}, que sirven para especificar los nombres de los atributos de la fuente de origen.

De forma adicional, se ha añadido un ContentUriRequestBody (proveniente de \href{https://gist.github.com/cketti/8ac927509787d7085a5ef8f866806f0f}{este GitHub Gist}) para poder enviar correctamente las imágenes que se escogen desde el selector de archivos, ya que utilizan el tipo de URI \textit{content://}, que no es compatible por Retrofit por defecto.

\subsection{Repositorio}

Aquí es donde empieza lo más interesante. Como vimos en la sección anterior, los repositorios son los que se encargan de ofrecer un único punto de entrada tanto a los componentes de Retrofit como los de Room. En este caso, solo contamos con dos repositorios: \textit{LoginDto}, para la funcionalidad de inicio de sesión y registro, y \textit{OutfitDto}, para todas las operaciones relacionadas con los outfits.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.35\textwidth]{figures/android-patrones-arquitectura/repository.png}
	\caption{Repositorios de la aplicación}
	\label{fig:repoClasses}
\end{figure}

Veamos un método sencillo del repositorio de los outfits, el que obtiene los outfits guardados en la base de datos local.

\begin{lstlisting}
fun getOutfits(): LiveData<Resource<List<OutfitDomain>>> {
	val liveData = MutableLiveData<Resource<List<OutfitDomain>>>()
	launch {
		val outfits = dao.getAll()
		if (outfits.isNotEmpty()) {
			liveData.postValue(Resource.Success(outfits))
		} else {
			liveData.postValue(Resource.Error(ErrorEnum.NO_OUTFITS))
		}
	}
	return liveData
}
\end{lstlisting}

En primer lugar se puede observar que, como era de esperar, el método devuelve un LiveData, pero este LiveData no es de una lista de outfits, sino de un Resource de una lista de outfits. Esta clase Resource, que se puede encontrar en el paquete \textit{utils}, es una forma inteligente de poder enviar tipos que pueden ser diferentes según la situación. Es una \textit{sealed class}, que se utilizan para representar jerarquías de clases restringidas, es decir, cuando un valor puede tener uno de los tipos de un conjunto limitado, pero no puede tener ningún otro tipo.

\begin{lstlisting}
sealed class Resource<T>(
	val data: T? = null,
	val message: String? = null,
	val error: ErrorEnum? = null
) {
	class Success<T>(data: T? = null) : Resource<T>(data)
	class Loading<T>(data: T? = null) : Resource<T>(data)
	class Error<T>(error: ErrorEnum? = null, message: String? = null, data: T? = null) : Resource<T>(data, message, error)
}
\end{lstlisting}

Un resource puede ser Loading, que quiere decir que el recurso se está cargando (esto se puede observar desde la vista para, por ejemplo, mostrar una animación de cargado), Success, que quiere decir que el recurso ya está disponible, y Error que puede mandar opcionalmente un ErrorEnum, un enumerado que se encuentra en \textit{utils} y contiene los distintos errores que puede producir la aplicación, como un error de conexión al servidor, o un error de validación de campos. Por ejemplo, en el método de inicio de sesión se pueden ver los distintos errores posibles al iniciar sesión, y cómo se emite un error distinto según qué código de respuesta haya devuelto el servidor:

\begin{lstlisting}
when (response.code()) {
	400 -> liveData.postValue(Resource.Error(ErrorEnum.VALIDATIONS_FAILED))
	401 -> liveData.postValue(Resource.Error(ErrorEnum.INCORRECT_PASSWORD))
	404 -> liveData.postValue(Resource.Error(ErrorEnum.NOT_REGISTRERED))
	else -> liveData.postValue(Resource.Error(ErrorEnum.SERVER_ERROR))
}
\end{lstlisting}

Gracias al uso de la clase Resource, desde la vista podemos observar los cambios en el LiveData, y mediante un \textit{when} comprobar si lo que hemos pedido está cargando, se ha cargado, o se ha producido un error, y ejecutar los métodos correspondientes.

Continuando con el método \textit{getOutfits()}, vemos que crea un LiveData vacío y lo devuelve, pero entre medio cuenta con el método \textit{launch}. Este método es aportado por CoroutineScope y se encarga de ejecutar el código en una corrutina. Como se ha explicado anteriormente, una corrutina nos permite ejecutar código en un hilo distinto al principal, para evitar bloqueos en la aplicación. Mediante el override de \textit{CoroutineContext} podemos configurar el comportamiento por defecto de las corrutinas:

\begin{lstlisting}
override val coroutineContext: CoroutineContext
	get() = job + Dispatchers.IO
\end{lstlisting}

En este caso, utilizamos el contexto Dispatchers.IO, que está optimizado para tareas que impliquen una entrada y salida, como lectura y escritura en la memoria o la conexión con una API, por lo que es perfecto para este repositorio. Una gran ventaja de las corrutinas es que cuenta con diversos \textit{Dispatchers} para que la corrutina se ejecute de la forma más optimizada posible según el tipo de tarea que realice.

Dentro de la corrutina, simplemente se hace una llamada a la base de datos (mediante el DAO), y una vez obtenidos los outfits se realiza un \textit{postValue} del Resource de la lista obtenida, que notificará a todo lo que esté observando a este LiveData para realizar las acciones oportunas, en este caso ocultar la animación de carga y mostrar los outfits en el RecyclerView.

El método más complejo del repositoro es \textit{getOutfitsFromApi()}, ya que dentro de su corrutina primero hace la llamada a la API por Retrofit, que se ejecuta en una \textit{suspend function} gracias al método \textit{awaitResponse()} ofrecido por esta librería, para posteriormente convertir la lista de OutfitDto obtenida a una lista de OutfitDomain para poder guardarla en la base de datos local, teniendo en cuenta las distintas respuestas posibles del servidor para enviar el ErrorEnum correspondiente y poder mostrar en la vista el error concreto que ha ocurrido y no uno genérico.

\subsection{ViewModel}

En la implementación de la app se ha decidido usar un ViewModel por cada vista. En la siguiente figura se pueden ver todos los ViewModel con los que cuenta la aplicación.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.45\textwidth]{figures/android-patrones-arquitectura/viewmodel.png}
	\caption{ViewModels de la aplicación}
	\label{fig:viewModelClasses}
\end{figure}

En cada ViewModel se inicializan los atributos requeridos por el repositorio que no puedan ser inicializados allí. Esto se hace porque los ViewModel pueden extender de AndroidViewModel y acceder al contexto de la aplicación, algo necesario, por ejemplo, para poder obtener la instanciación del DAO o acceder a las Shared Preferences donde están los datos de inicio de sesión. Además, se inicializa un job que se pasa al repositorio para el CoroutineContext, que permite que cuando el estado del ViewModel sea \textit{cleared} el proceso sea cancelado de forma segura. Aunque gracias a los Architecture Components la aplicación no crashearía, el poder cancelar las corrutinas cuando se acabe el ciclo de vida del ViewModel hace que no continúe la ejecución de código que ya no es necesario.

Finalmente, el ViewModel cuenta con los distintos métodos del que haga uso la vista, que solo llaman al método correspondiente del repositorio, por ejemplo:

\begin{lstlisting}
fun getOutfits(): LiveData<Resource<List<OutfitDomain>>> {
	return outfitRepository.getOutfits()
}
\end{lstlisting}

\subsection{Activities y Fragments}

La implementación del patrón de arquitectura termina en la vista. En ella, primero se ha de inicializar el ViewModel, para lo que se hace uso del conveniente \textit{by lazy} de Kotlin, que inicializa una variable la primera vez que se acceda a ella. La inicialización del ViewModel se realiza mediante ViewModelProvider, que se encarga de que el ciclo del ViewModel se adapte al de la vida en cuestión.

\begin{lstlisting}
private val outfitsListViewModel by lazy { ViewModelProvider(this).get(OutfitsListViewModel::class.java) }
\end{lstlisting}

En el método \textit{onViewCreated()} u \textit{onCreate()}, según se trate de un Fragment o una Activity, es donde podemos ya ejecutar los métodos del ViewModel y observarlos:

\begin{lstlisting}
outfitsListViewModel.getOutfitsFromApi().observe(viewLifecycleOwner, outfitsListObserver)
\end{lstlisting}

Para poder observarlo, necesitamos definir con anterioridad un observer, que es donde añadiremos toda la lógica a ejecutar al recibir las actualizaciones del LiveData que nos ha devuelto el método, que mediante el uso de \textit{when} definimos los distintos casos según el recurso sea Loading, Success o Error. Por ejemplo, este es el observer usado en la lista principal de outfits:

\begin{lstlisting}
private val outfitsListObserver: Observer<Resource<List<OutfitDomain>>> = Observer { resource ->
	when (resource) {
		is Resource.Loading -> {
			setLoadingIndicator(true)
			resource.data?.let { outfits ->
				adapter.setOutfits(outfits)
			}
		}
		is Resource.Success -> resource.data?.let { outfits ->
			adapter.setOutfits(outfits)
			setLoadingIndicator(false)
		}
		is Resource.Error -> {
			when (resource.error) {
				ErrorEnum.SERVER_ERROR -> showSnackbar(R.string.error__server)
				ErrorEnum.RETROFIT_ERROR -> showSnackbar(R.string.error__retrofit)
				ErrorEnum.NO_OUTFITS -> adapter.setOutfits(mutableListOf())
				else -> showSnackbar(R.string.error__unspecified)
			}
			resource.data?.let { outfits ->
				adapter.setOutfits(outfits)
			}
			setLoadingIndicator(false)
		}
	}
}
\end{lstlisting}

\section{Conclusiones y experiencia personal}

El uso de Architecture Components y su patrón recomendado ha permitido que la aplicación de Fashionlint sea robusta y cuente con un código muy limpio y mantenible. La experiencia al aplicar este patrón de arquitectura, aunque al principio supuso un esfuerzo comprenderla, ha sido muy satisfactoria, y, en mi humilde opinión, tanto Kotlin como los Architecture Components han marcado un antes y un después en el panorama del desarrollo Android, haciendo que crear una aplicación que tenga en cuenta el ciclo de vida sea más fácil e intuitivo que antes.