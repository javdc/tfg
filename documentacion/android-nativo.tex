%!TEX root =  tfg.tex
\chapter{Desarrollo nativo: Android Studio y Kotlin}

\begin{quotation}[Software Engineer]{Grady Booch (1955--nuestros días)}
The task of the software development team is to engineer the illusion of simplicity.
\end{quotation}

\begin{abstract}
	En este capítulo se hará una pequeña introducción al desarrollo nativo en Android y al lenguaje de programación Kotlin y sus ventajas.
\end{abstract}

\section{Introducción al desarrollo nativo en Android}

El entorno de desarrollo oficial para Android es Android Studio. Lanzado en 2014, este IDE basado en IntelliJ IDEA está diseñado específicamente para desarrollar aplicaciones nativas de Android, reemplazando a Eclipse y el plugin Android Development Tools, que dejó de recibir soporte oficial por parte de Google en 2015.

Aunque inicialmente el lenguaje de programación soportado principalmente en Android Studio y recomendado por Google era Java, en 2017 se anunció el soporte oficial del lenguaje Kotlin, y desde la versión 3.0 de Android Studio se \href{(https://blog.jetbrains.com/kotlin/2017/05/kotlin-on-android-now-official/}{ofreció soporte para este novedoso lenguaje de programación}. Además, desde 2019 Kotlin se ha convertido en el \href{http://social.techcrunch.com/2019/05/07/kotlin-is-now-googles-preferred-language-for-android-app-development/}{lenguaje de programación principal}, dejando Java en un segundo plano. A continuación veremos con más detalle en qué consiste este reciente lenguaje de programación y qué ventajas ofrece frente a Java, un lenguaje con muchos años de madurez y confiado por millones de programadores.

\section{¿Qué es Kotlin?}

Kotlin es un lenguaje de programación multiplataforma, de tipado estático y de propósito general con inferencia de tipos. Está diseñado para interoperar completamente con Java, y la versión JVM de la biblioteca estándar de Kotlin depende de la Java Class Library, pero la inferencia de tipos permite que su sintaxis sea más concisa. Kotlin se dirige principalmente a la JVM, pero también compila a JavaScript (por ejemplo, para aplicaciones web de frontend usando React) o código nativo (vía LLVM), por ejemplo, para aplicaciones nativas de iOS compartiendo la lógica de negocio con aplicaciones de Android.

Este lenguaje de programación, cuya primera versión estable apareció en 2016, está desarrollado y financiado por JetBrains, creador de IntelliJ IDEA, un entorno de desarrollo en el cual se basa Android Studio, el IDE oficial para el desarrollo de aplicaciones nativas en Android.

\section{Principales ventajas de Kotlin}

% En su documentación oficial podemos encontrar algunas de las ventajas que ofrece este lenguaje de programación frente al ya conocido Java (https://kotlinlang.org/docs/reference/comparison-to-java.html).

Ya hemos visto que Kotlin es el lenguaje de programación recomendado por Google para el desarrollo nativo en Android. Sin embargo, ¿qué aporta Kotlin frente al ya conocido Java? Antonio Leiva, programador Android con experiencia que forma parte del programa Google Developer Experts, distingue en su libro Kotlin for Android Developers cuatro características principales que ofrecen una gran ventaja a Kotlin frente a Java en el contexto del desarrollo en Android: \cite{kotlinForAndroidDevelopers}

\begin{itemize}
	\item Expresividad
	\item Seguridad frente a nulos
	\item Funciones de extensión
	\item Programación funcional
\end{itemize}

A continuación veremos en detalle cada una de estas ventajas.

\subsection{Expresividad}

Kotlin hace mucho más sencillo evitar código ``boilerplate'', es decir, código repetido que podemos encontrar en distintos sitios con muy poca alteración, ya que Kotlin cubre los patrones de código más comunes por defecto.

Esto se puede comprender fácilmente con un ejemplo. Supongamos que tenemos que crear una clase Artista con cuatro atributos distintos. Si quisiéramos definir esta clase en Java, tendríamos, aparte de definir los atributos en sí, que crear los \textit{getters} y \textit{setters} necesarios y añadir otros métodos como \textit{getString()} o \textit{hashCode()} en caso de que fuesen necesarios. De esta forma, nuestro código sería algo similar a esto:

\lstset{language=Java}
\begin{lstlisting}
public class Artist {
	private long id;
	private String name;
	private String url;
	private String mbid;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getMbid() {
		return mbid;
	}
	
	public void setMbid(String mbid) {
		this.mbid = mbid;
	}
	
	@Override public String toString() {
		return "Artist{" +
				"id=" + id +
				", name='" + name + '\'' +
				", url='" + url + '\'' +
				", mbid='" + mbid + '\'' +
				'}';
	}
}
\end{lstlisting}

Este código tan largo se puede realizar en Kotlin de forma muy sencilla con un \textit{data class}:

\lstset{language=Kotlin}
\begin{lstlisting}
data class Artist(var id: Long, var name: String, var url: String, var mbid: String)
\end{lstlisting}

Aun con la considerable diferencia entre ambos códigos, ambos realizan la misma función, y, de hecho, el código compilado a JVM de ambos seguramente sea muy similar.

\subsection{Seguridad frente a nulos}

Cuando desarrollamos usando Java, una gran parte de nuestro código es defensivo, esto quiere decir que necesitamos comprobar continuamente si algo es nulo antes de usarlo si no queremos encontrar un NullPointerException inesperado. Kotlin, como muchos otros lenguajes modernos, es seguro frente a nulos porque hace necesario especificar explícitamente si un objeto puede ser nulo usando el operador de llamada segura (escrito ``?'').

Por ejemplo, si creamos un usuario que puede ser nulo, por ejemplo:

\begin{lstlisting}
var user: User? = null
\end{lstlisting}

E intentamos ejecutar algún código sobre esta variable directamente, por ejemplo obtener sus discos favoritos, este no compilaría, ya que la variable podría ser nula. Sin embargo, usando el operador ``?'' podemos ejecutar incluso varios métodos uno detrás de otro sin tener que considerar que cualquiera de los valores podría ser nulo. En el caso de que alguno lo sea, simplemente la variable a la que asignemos el resultado será nula también:

\begin{lstlisting}
var favoriteArtist = user?.getFavoriteAlbums()?.get(0)?.getArtist()
\end{lstlisting}

Realizar algo así en Java tomaría como mínimo varias líneas de código y múltiples comprobaciones mediante \textit{ifs}. Por esto, en Kotlin es prácticamente imposible provocar un error debido a que un valor sea nulo de forma inesperada.

\subsection{Funciones de extensión}

Kotlin hace especialmente sencillo añadir nuevas funciones a una clase, incluso a clases ya proporcionadas por el propio framework de Java o Android. Es un sustituto de las clases Utils que hacen el código mucho más legible e intuitivo y por tanto más fácil de mantener.

Por ejemplo, si tuviéramos que mostrar varios \textit{Toast} en distintos \textit{Fragments} de una aplicación, sería necesario escribir el siguiente código ajustando el mensaje y la duración a cada caso específico:

\begin{lstlisting}
Toast.makeText(getActivity(), "Hello world", Toast.LENGTH_LONG).show()
\end{lstlisting}

Sin embargo, en Kotlin podríamos definir un nuevo método al fragment para mostrar un \textit{Toast}, evitando la repetición innecesaria de código:

\begin{lstlisting}
fun Fragment.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT){
	Toast.makeText(getActivity(), message, duration).show()
}
\end{lstlisting}

Para posteriormente llamar a este método donde sea necesario de la siguiente forma:

\begin{lstlisting}
fragment.toast("Hello world")
\end{lstlisting}

\subsection{Soporte para programación funcional}

Aunque Java 8 también ofrece soporte para programación funcional con \textit{lambdas}, estas están bastante limitadas frente a Kotlin. Por ejemplo, implementar un \textit{click listener} a una vista de Android solo requeriría la siguiente línea de código:

\begin{lstlisting}
view.setOnClickListener { toast("Hello world!") }
\end{lstlisting}

\subsection{Otras ventajas de Kotlin}

Kotlin ofrece una gran cantidad de métodos que permiten realizar tareas de Java normalmente tediosas de forma mucho más sencilla. Este tipo de sintaxis adicional, llamada comúnmente \textit{syntactic sugar}, hacen que el código escrito en Kotlin, manteniendo la misma funcionalidad que su equivalente en Java, sea mucho más legible, y por tanto más mantenible y menos propenso a errores.

\section{Potencial de Kotlin en el futuro}

Las ventajas de Kotlin no acaban aquí, el hecho de que Kotlin sea compilable no solo a JVM, sino a más plataformas, hace que tenga un gran potencial no solo para el desarrollo de aplicaciones nativas de Android, sino también para el desarrollo de aplicaciones web o incluso multiplataforma con un mismo código.

Mediante Kotlin/Native, Jetbrains busca crear un lenguaje de programación que, ofreciendo las ventajas del código nativo como su rendimiento superior, también ofrezca la flexibilidad de poder ser reutilizable en distintas plataformas. Kotlin/Native es una tecnología para compilar código Kotlin a binarios nativos, que pueden funcionar sin una máquina virtual. Su compilador está basado en LLVM y está diseñado principalmente para permitir la compilación para plataformas en las que las máquinas virtuales no son lo más óptimo o no pueden usarse, por ejemplo, dispositivos embebidos o iOS. Kotlin/Native actualmente es compatible con iOS, macOS, watchOS, tvOS, Android, Windows, Linux y WebAssembly.\cite{kotlinNative}